﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MainResource.Classes
{
    public class Clothes
    {
        public int Slot { get; set; }
        public int Drawable { get; set; }
        public int Texture { get; set; }

        public Clothes(int slot, int drawable, int texture)
        {
            Slot = slot;
            Drawable = drawable;
            Texture = texture;
        }
    }
}
