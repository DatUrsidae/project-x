﻿using GTANetworkAPI;
using MainResource.Enums;
using MainResource.Models.PlayerData;
using System;
using System.Linq;

namespace MainResource.Classes.Missions
{
    internal enum MissionAction
    {
        None = 0,
        Robbery = 1,
        Vandalism = 2
    }

    internal class Vandalism
    {
        public bool Vandalising;
    }

    internal class Robbery
    {
        public bool Robbing;
    }
    internal class Objective : Mission
    {
        internal string ObjectiveName { get; set; }
        internal Vector3 Location { get; set; }
        internal uint Reward { get; set; }
        internal Objectives ObjectiveType { get; set; }
        internal MissionAction Action { get; set; }
        internal Vandalism Vandalism { get; set; } = new Vandalism();
        internal Robbery Robbery { get; set; } = new Robbery();

        internal Objective NewObjective(Objectives type)
        {
            ObjectiveType = type;
            ObjectiveName = ObjectiveType.ToString();
            return this;
        }

        internal void ObjAction(Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            switch (Action)
            {
                case MissionAction.Robbery:
                    if (!Robbery.Robbing)
                    {
                        player.Client.TriggerEvent("Progress:Start", 10, "Robbery");
                        Robbery.Robbing = true;
                    }
                    break;
                case MissionAction.Vandalism:
                    if (!Vandalism.Vandalising)
                    {
                        player.Client.TriggerEvent("Progress:Start", 10, "Vandalism");
                        Vandalism.Vandalising = true;
                    }
                    break;
                default:
                    break;
            }
        }

        [RemoteEvent("Robbery")]
        internal void Rob(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            player.Mission.SendNotificationToPlayers("Mission", "Robbed this shit!", "secondary");
            player.Mission.ResetPlayerDatas("Mission:Rob");
            Robbery.Robbing = false;
            player.Mission.NextMission();
        }

        [RemoteEvent("Vandalism")]
        internal void Van(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            player.Mission.SendNotificationToPlayers("Mission", "Vandalized this shit!", "secondary");
            player.Mission.ResetPlayerDatas("Mission:Van");
            Vandalism.Vandalising = false;
            player.Mission.NextMission();
        }

        public void ObjTrigger(Player player)
        {
            switch (Action)
            {
                case MissionAction.Robbery:
                    ObjAction(player.Client);
                    break;
                case MissionAction.Vandalism:
                    ObjAction(player.Client);
                    break;
                default:
                    break;
            }
        }

        public void Spawn(Player player)
        {
            switch (ObjectiveType)
            {
                case Enums.Objectives.Robbery:
                    Console.WriteLine("List count: " + Global.RobberyCoords.Count);
                    Console.WriteLine("Random number: " + Global.Random.Next(0, Global.RobberyCoords.Count));
                    Location = Global.RobberyCoords[Global.Random.Next(0, Global.RobberyCoords.Count)];
                    var closestRob = NAPI.Pools.GetAllColShapes().Min(a => a.Position.DistanceTo(Location));
                    var robShape = NAPI.Pools.GetAllColShapes().Where(a => a.Position.DistanceTo(Location) == closestRob).FirstOrDefault();
                    player.Client.SetData("Mission:Rob", robShape.GetData("Mission:Rob"));
                    Action = MissionAction.Robbery;
                    Reward = 500;
                    break;
                case Enums.Objectives.Vandalism:
                    Console.WriteLine("List count: " + Global.RobberyCoords.Count);
                    Console.WriteLine("Random number: " + Global.Random.Next(0, Global.RobberyCoords.Count));
                    Location = Global.VandalismCoords[Global.Random.Next(0, Global.VandalismCoords.Count)];
                    var closestVan = NAPI.Pools.GetAllColShapes().Min(a => a.Position.DistanceTo(Location));
                    var vanShape = NAPI.Pools.GetAllColShapes().Where(a => a.Position.DistanceTo(Location) == closestVan).FirstOrDefault();
                    player.Client.SetData("Mission:Van", vanShape.GetData("Mission:Rob"));
                    Action = MissionAction.Vandalism;
                    Reward = 250;
                    break;
                default:
                    break;
            }
        }
    }
}
