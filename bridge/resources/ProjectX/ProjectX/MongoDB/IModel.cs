﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MainResource.MongoDB
{
    public interface IModel
    {
        string CollectionName { get; set; }

        Guid ID { get; set; }
    }
}