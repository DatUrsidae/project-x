﻿using GTANetworkAPI;
using MainResource.Classes;
using System;
using System.Collections.Generic;

namespace MainResource.MongoDB.Models.VehicleData
{
    internal class VehicleInfo
    {
        internal Guid VehicleID { get; set; } = Guid.Empty;
        internal Vehicle Vehicle { get; set; } = null;
        internal uint InventorySize { get; set; } = 0;
        internal List<Loot> Inventory { get; set; } = new List<Loot>();

        internal void SetInventorySize(Vehicle vehicle)
        {
            switch (vehicle.Class)
            {
                case 0:
                    InventorySize = 2;
                    break;
                case 1:
                    InventorySize = 3;
                    break;
                case 2:
                    InventorySize = 4;
                    break;
                case 3:
                    InventorySize = 3;
                    break;
                case 4:
                    InventorySize = 3;
                    break;
                case 5:
                    InventorySize = 3;
                    break;
                case 6:
                    InventorySize = 3;
                    break;
                case 7:
                    InventorySize = 2;
                    break;
                case 8:
                    InventorySize = 1;
                    break;
                case 9:
                    InventorySize = 3;
                    break;
                case 10:
                    InventorySize = 4;
                    break;
                case 11:
                    InventorySize = 4;
                    break;
                case 12:
                    InventorySize = 5;
                    break;
                case 13:
                    InventorySize = 0;
                    break;
                default:
                    InventorySize = 0;
                    break;
            }
        }
    }
}
