﻿using GTANetworkAPI;
using MainResource.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MainResource.Models.Houses
{
    internal class House : IModel
    {
        [BsonIgnore]
        public string CollectionName { get; set; } = "houses";
        [BsonId]
        public Guid ID { get; set; }
        [BsonElement("ownerid")]
        internal Guid OwnerId { get; set; }
        [BsonElement("owner")]
        internal string Owner { get; set; }
        [BsonElement("street")]
        internal string Street { get; set; }
        [BsonElement("location")]
        internal string Location { get; set; }
        [BsonElement("price")]
        internal uint Price { get; set; }
        [BsonElement("entry")]
        internal Vector3 Entry { get; set; }
        [BsonElement("interior")]
        internal string Interior { get; set; }
        [BsonElement("forsale")]
        internal bool ForSale { get; set; }
    }
}
