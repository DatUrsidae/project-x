﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Enums;

namespace MainResource.Features.Professions.Civilian.Mechanic
{
    class Mechanic : Script
    {
        [Command("repair")]
        internal void Repair(Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            if (player.Team.Specialization == TeamSpecialization.Mechanic)
            {
                var cars = NAPI.Pools.GetAllVehicles();

                foreach(Vehicle veh in cars)
                {
                    if (veh.Position.DistanceTo2D(client.Position) < 4f && !client.IsInVehicle)
                    {
                        veh.Repair();
                        client.SendNotification($"You have repaired ~y~{veh.DisplayName}~w~.");
                        player.GiveExp(1500);
                        return;
                    }
                }
            }
        }
    }
}
