﻿using MainResource.Models.PlayerData;
using System;
using System.Collections.Generic;
using GTANetworkAPI;
using MainResource.Teams;
using MainResource.Handler;
using MainResource.Utility;
using MainResource.Classes;
using MainResource.Enums;

namespace MainResource.Features.Professions.Criminal
{
    internal class PropertyRob : ServerEvents
    {
        internal PropertyRob()
        {
            ColShape col = NAPI.ColShape.CreateCylinderColShape(new Vector3(893.436f, -889.5112f, 27.05505f), 2, 2);
            col.SetData("Property:DropOff", null);
            new Property("Sound Sanctuary", new Vector3(386.9616f, -969.8793f, 29.44273f), new List<Vector3>().AddMany(new Vector3(388.9771f, -971.7824f, 29.44131f), new Vector3(390.2511f, -969.694f, 29.4438f), new Vector3(388.6946f, -967.8771f, 29.44594f)));
            new Property("Wholesale Mart", new Vector3(371.4796f, -941.7711f, 29.43681f), new List<Vector3>().AddMany(new Vector3(368.3329f, -944.4385f, 29.43577f), new Vector3(371.9417f, -945.3942f, 29.437f), new Vector3(376.7031f, -943.7048f, 29.43409f), new Vector3(372.8217f, -942.6058f, 29.4364f)));
            new Property("Bean Machine Coffee", new Vector3(125.3785f, -1027.928f, 29.35735f), new List<Vector3>().AddMany(new Vector3(128.514f, -1026.797f, 29.35738f), new Vector3(127.8885f, -1025.01f, 29.35749f), new Vector3(125.1152f, -1026.174f, 29.35744f)));
            new Property("Ground & Pound", new Vector3(54.87864f, -799.486f, 31.58714f), new List<Vector3>().AddMany(new Vector3(57.68016f, -798.3125f, 31.57535f), new Vector3(53.74094f, -797.1752f, 31.58928f), new Vector3(55.14257f, -796.0544f, 31.60101f), new Vector3(60.03358f, -795.1408f, 31.64404f)));
            new Property("Yogarishima", new Vector3(-521.2753f, -855.6256f, 30.24436f), new List<Vector3>().AddMany(new Vector3(-518.1591f, -855.5748f, 30.35902f), new Vector3(-520.6462f, -853.2133f, 30.33132f), new Vector3(-517.7673f, -851.442f, 30.41278f), new Vector3(-524.7946f, -854.1207f, 30.16943f)));
            new Property("Wook Noodle House", new Vector3(-655.9249f, -880.1505f, 24.6949f), new List<Vector3>().AddMany(new Vector3(-652.9973f, -880.124f, 24.58426f), new Vector3(-653.7858f, -877.9534f, 24.52436f), new Vector3(-651.6454f, -882.4959f, 24.66426f)));
            new Property("Pops Pills", new Vector3(103.2609f, -2.272199f, 68.00032f), new List<Vector3>().AddMany(new Vector3(100.8986f, -3.244154f, 68.09603f), new Vector3(102.3723f, -5.238377f, 68.04711f), new Vector3(103.841f, -4.286988f, 68.02407f), new Vector3(101.2259f, -6.274692f, 68.05132f)));
            new Property("Hawaiian Snow", new Vector3(253.1523f, -205.8862f, 54.11035f), new List<Vector3>().AddMany(new Vector3(250.2786f, -204.0497f, 54.14208f), new Vector3(249.7955f, -206.3532f, 53.99517f), new Vector3(250.9334f, -208.3176f, 53.98335f), new Vector3(253.3193f, -209.6087f, 53.98014f)));
            new Property("Porncrackers", new Vector3(219.1441f, -1531.811f, 29.2913f), new List<Vector3>().AddMany(new Vector3(222.1817f, -1531.627f, 29.32161f), new Vector3(220.8204f, -1529.911f, 29.30748f), new Vector3(218.9401f, -1528.829f, 29.29091f), new Vector3(221.3071f, -1527.01f, 29.15834f)));
            new Property("Greenback", new Vector3(-602.1436f, -1125.011f, 22.32425f), new List<Vector3>().AddMany(new Vector3(-600.395f, -1126.828f, 22.32425f), new Vector3(-600.1437f, -1124.601f, 22.32425f), new Vector3(-601.0414f, -1123.097f, 22.32425f), new Vector3(-596.5142f, -1128.233f, 22.18066f)));
            new Property("Chicos Supermarket", new Vector3(1088.519f, -776.5444f, 58.33813f), new List<Vector3>().AddMany(new Vector3(1090.994f, -774.0272f, 58.19477f), new Vector3(1088.956f, -772.6334f, 57.96764f), new Vector3(1087.146f, -774.7974f, 58.24364f), new Vector3(1086.026f, -773.0385f, 58.02069f)));
            new Property("Better Oral", new Vector3(1154.891f, -452.6917f, 66.98434f), new List<Vector3>().AddMany(new Vector3(1153.248f, -453.9002f, 66.98434f), new Vector3(1154.551f, -455.8493f, 66.98434f), new Vector3(1156.481f, -454.914f, 66.98434f), new Vector3(1156.141f, -457.405f, 66.98434f), new Vector3(1151.812f, -456.2906f, 66.98434f)));
            new Property("Gems Jewellery", new Vector3(226.1644f, 367.2789f, 106.0796f), new List<Vector3>().AddMany(new Vector3(224.1141f, 366.7195f, 106.0884f), new Vector3(224.4668f, 364.5216f, 106.0365f), new Vector3(226.7612f, 363.9319f, 105.9769f), new Vector3(227.8687f, 365.5287f, 105.9852f)));
            new Property("Hardcore Comic Store", new Vector3(-143.5177f, 229.4818f, 94.93461f), new List<Vector3>().AddMany(new Vector3(-141.9547f, 230.7513f, 94.94765f), new Vector3(-143.6243f, 232.3761f, 94.95854f), new Vector3(-145.2603f, 231.5874f, 94.94954f)));
            new Property("Binco", new Vector3(-487.1302f, 276.7726f, 83.32397f), new List<Vector3>().AddMany(new Vector3(-488.6711f, 275.3611f, 83.27465f), new Vector3(-487.125f, 273.9778f, 83.24549f), new Vector3(-484.7802f, 274.4289f, 83.25822f)));
            new Property("XYZ Store", new Vector3(-1446.568f, -184.9098f, 47.44727f), new List<Vector3>().AddMany(new Vector3(-1446.311f, -187.3522f, 47.49303f), new Vector3(-1444.04f, -187.3777f, 47.42127f), new Vector3(-1442.891f, -184.8052f, 47.45474f)));
            new Property("Los Santos Office Supply", new Vector3(-49.63411f, -212.3069f, 45.80523f), new List<Vector3>().AddMany(new Vector3(-51.30434f, -212.9592f, 45.80531f), new Vector3(-49.49394f, -214.1438f, 45.80367f), new Vector3(-47.37934f, -213.6948f, 45.80384f)));
            new Property("Gavs Cash Exchange", new Vector3(420.7072f, -1893.051f, 26.13767f), new List<Vector3>().AddMany(new Vector3(418.8427f, -1891.15f, 26.14517f), new Vector3(421.4218f, -1890.564f, 26.25453f), new Vector3(417.6502f, -1893.714f, 26.02441f)));
        }

        internal static void StartRobbing(ColShape colShape, Player player)
        {
            if (colShape.HasData("Property") && player.VehicleInfo.Vehicle != null && player.Team.TeamEnum == TeamEnum.Criminal)
            {
                var property = (Property)colShape.GetData("Property");
                if ((DateTime.UtcNow - property.LastRobbed).TotalMinutes <= 1)
                {
                    player.SendNotification("Property", $"This property was already robbed recently!", "warning");
                    return;
                }
                if (player.Client.HasData("Property"))
                {
                    player.Client.TriggerEvent("PropertyRob:DeleteObjects");
                    player.Client.ResetData("Property");
                }
                property.LastRobbed = DateTime.UtcNow;
                player.Client.PlayAnimation("weapons@projectile@grenade_str", "throw_h_fb_backward", (int)(AnimationFlags.Loop | AnimationFlags.OnlyAnimateUpperBody));
                NAPI.Task.Run(() =>
                {
                    player.Client.StopAnimation();
                    player.Client.TriggerEvent("BreakWindowSound");
                    player.Client.TriggerEvent("PropertyRob:Broke", player.VehicleInfo.Vehicle, property.ObjectSpawns);
                    player.Client.SetData("Property", property);
                }, delayTime: 5000);
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("Property") && player.VehicleInfo.Vehicle != null && player.Team.TeamEnum == TeamEnum.Criminal)
            {
                player.Client.SetData("eDown:Action", Actions.PropertyRob);
            }

            if (colShape.HasData("Property:DropOff") && player.VehicleInfo.Inventory.Count > 0 && player.VehicleInfo.Vehicle != null && player.Client.Vehicle == player.VehicleInfo.Vehicle && player.Team.TeamEnum == TeamEnum.Criminal)
            {
                uint reward = 0;
                foreach (var loot in player.VehicleInfo.Inventory)
                {
                    reward += loot.Worth;
                }
                Loot.RemoveLoot(player.Client);
                player.AddCredits(reward);
                player.SendNotification("Loot Drop Off", $"Your loot was worth {reward}$ and it has been added into your balance", "success");
                player.GiveExp(100);
                player.GiveTeamExp(150);
                player.Client.TriggerEvent("PropertyRob:DroppedOff");
            }   
        }
    }

    internal class PropertyRemotes : Script
    {
        [RemoteEvent("AddLoot")]
        internal void SetLoot(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.VehicleInfo.Inventory.Count == player.VehicleInfo.InventorySize)
            {
                player.SendNotification("Vehicle Inventory", $"Your vehicle inventory is full!", "danger");
                return;
            }
            if (player.VehicleInfo.Vehicle != null)
            {
                var name = (string)args[0];
                var worth = (int)args[1];
                player.VehicleInfo.Inventory.Add(new Loot(name, Convert.ToUInt32(worth), client));
                player.SendNotification("Vehicle Inventory", $"Added Loot: \\ Item: {name} \\ Item Worth: {worth}$", "success");
                player.SendNotification("Vehicle Inventory", $"Inventory Count: {player.VehicleInfo.Inventory.Count} out of {player.VehicleInfo.InventorySize}.", "secondary");
                if (player.VehicleInfo.Inventory.Count == 1)
                {
                    client.TriggerEvent("PropertyRob:DropOff");
                    player.SendNotification("Loot Delivery", $"Go to the drop-off or collect more!", "secondary");
                }
            }
        }

        [RemoteEvent("Property:Clear")]
        internal void PropertyClear(Client client)
        {
            if (client.HasData("Property"))
            {
                client.ResetData("Property");
            }
        }

        [RemoteEvent("CarryAnim")]
        internal void CarryAnim(Client client, bool play)
        {
            if (client.IsInVehicle) return;
            if (play) client.PlayAnimation("anim@heists@box_carry@", "walk", (int)(Enums.AnimationFlags.Loop | Enums.AnimationFlags.AllowPlayerControl | Enums.AnimationFlags.OnlyAnimateUpperBody));
            else if (!play) client.StopAnimation();
        }
    }
}
