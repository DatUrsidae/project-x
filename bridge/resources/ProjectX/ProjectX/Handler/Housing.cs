﻿using GTANetworkAPI;
using MainResource.Models.Houses;
using MainResource.Models.PlayerData;
using System;
using System.Collections.Generic;
using MainResource.Models;
using Newtonsoft.Json;

namespace MainResource.Handler
{
    internal class Housing : ServerEvents
    {
        string Street { get; set; }
        string Location { get; set; }
        Dictionary<string, Vector3> Interiors = new Dictionary<string, Vector3>
        {
            ["Motel"] = new Vector3(152.2605, -1004.471, -98.99999),
            ["LowEnd"] = new Vector3(261.4586, -998.8196, -99.00863),
            ["MedEnd"] = new Vector3(347.2686, -999.2955, -99.19622)
        };
        
        [Command("createhouse")]
        internal void CreateHouse(Client client, uint price, string interior)
        {
            if (!client.GetPlayer(out var player)) return;
            client.TriggerEvent("GetStreet");
            NAPI.Task.Run(() =>
            {
                DatabaseHandler.CreateHouse(Street, Location, price, client.Position, interior);
                player.SendNotification($"House Added", $"{Street} {Location} {price}", "success");
            }, delayTime: 500);
           
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("House"))
            {
                if (colShape.HasData("eDown:UI"))
                {
                    player.Client.SetData($"eDown:UI", colShape.GetData("eDown:UI"));
                }
            }
            
            if (colShape.HasData("HouseExit") && player.PlayerInfo.House != null)
            {
                player.Client.Position = player.PlayerInfo.House.Entry;
                player.Client.Dimension = 0;
                player.PlayerInfo.House = null;
            }
        }

        internal override void OnResourceStart()
        {
            var lowEndApt = NAPI.ColShape.CreateCylinderColShape(new Vector3(266.1611, -1007.274, -101.0085), 0.5f, 1);
            lowEndApt.SetData("HouseExit", null);
            NAPI.Marker.CreateMarker(20, new Vector3(266.1611, -1007.274, -101.0085), new Vector3(), new Vector3(), 1, new Color(200, 200, 0, 100));
            var motel = NAPI.ColShape.CreateCylinderColShape(new Vector3(151.3538, -1007.705f, -98.99998f), 0.5f, 1);
            motel.SetData("HouseExit", null);
            NAPI.Marker.CreateMarker(20, new Vector3(151.3538, -1007.705f, -98.99998f), new Vector3(), new Vector3(), 1, new Color(200, 200, 0, 100));
            var medEndApt = NAPI.ColShape.CreateCylinderColShape(new Vector3(346.5265f, -1012.783f, -99.19622f), 0.5f, 1);
            medEndApt.SetData("HouseExit", null);
            NAPI.Marker.CreateMarker(20, new Vector3(346.5265f, -1012.783f, -99.19622f), new Vector3(), new Vector3(), 1, new Color(200, 200, 0, 100));
        }

        [RemoteEvent("GetHouse")]
        internal void GetHouse(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.ColShape.HasData("House"))
            {
                var house = (House)player.ColShape.GetData("House");
                client.TriggerEvent("house:getHouse", house.Street, house.Location, house.Price, house.Owner, house.ID.ToString(), house.ForSale.ToString());
            }
        }

        [RemoteEvent("Streets")]
        internal void Streets(Client client, object[] args)
        {
            Street = (string)args[0];
            Location = (string)args[1];
        }

        [RemoteEvent("GetInHouse")]
        internal void GetInHouse(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.ColShape != null && player.ColShape.HasData("House"))
            {
                var house = (House)player.ColShape.GetData("House");
                if (house.OwnerId != player.ID) return;
                var interior = Interiors.GetValueOrDefault(house.Interior);
                if (interior == null)
                {
                    client.SendChatMessage("Could not find a interior.");
                    return;
                }
                player.PlayerInfo.House = house;
                player.Client.Dimension = Convert.ToUInt32(client.Value) + 1;
                player.Client.Position = interior;
                player.Client.TriggerEvent("house:Close");
            }
        }

        [RemoteEvent("BuyHouse")]
        internal void BuyHouse(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            var houseId = (string)args[0];
            var price = Convert.ToInt32(args[1]);
            var objGuid = JsonConvert.DeserializeObject(houseId);
            var guid = Guid.Parse(objGuid.ToString());
            if (player.ColShape.HasData("House"))
            {
                var pHouse = (House)player.ColShape.GetData("House");
                if (pHouse.OwnerId == player.ID)
                {
                    player.SendNotification("House", "You already own this house!", "warning");
                    return;
                }

                if (!pHouse.ForSale)
                {
                    player.SendNotification("House", "This house is not for sale!", "danger");
                    return;
                }

                if (player.PlayerInfo.Credits < price)
                {
                    player.SendNotification("House", "You dont have enough money to buy this house!", "danger");
                    return;
                }

                DatabaseHandler.BuyHouse(player, guid);
                pHouse.Owner = player.Client.Name;
                pHouse.OwnerId = player.ID;
                player.PlayerInfo.Credits -= Convert.ToUInt32(price);
                player.SendNotification("House", $"You have purchased this house in: {pHouse.Location}, {pHouse.Street} for {price}.", "success");
                client.TriggerEvent("house:getHouse", pHouse.Street, pHouse.Location, pHouse.Price, pHouse.Owner, pHouse.ID.ToString(), pHouse.ForSale);
            }
        }

        [RemoteEvent("SellHouse")]
        internal void SellHouse(Client client, string houseId)
        {
            if (!client.GetPlayer(out var player)) return;
            var objGuid = JsonConvert.DeserializeObject(houseId);
            var guid = Guid.Parse(objGuid.ToString());
            if (player.ColShape.HasData("House"))
            {
                var pHouse = (House)player.ColShape.GetData("House");

                if (pHouse.OwnerId != player.ID)
                {
                    player.SendNotification("House", "You dont own this house!", "danger");
                    return;
                }
                if (pHouse.ForSale)
                {
                    pHouse.ForSale = false;
                    DatabaseHandler.SellHouse(guid, false);
                    player.SendNotification($"House", $"{pHouse.Street} at {pHouse.Location} is now set not for sale!", "success");
                }
                else if (!pHouse.ForSale)
                {
                    pHouse.ForSale = true;
                    DatabaseHandler.SellHouse(guid, true);
                    player.SendNotification($"House", $"{pHouse.Street} at {pHouse.Location} is now set for sale!", "success");
                }
                client.TriggerEvent("house:getHouse", pHouse.Street, pHouse.Location, pHouse.Price, pHouse.Owner, pHouse.ID.ToString(), pHouse.ForSale.ToString());
            }
        }
    }
}
