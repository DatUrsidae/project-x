﻿using GTANetworkAPI;

namespace MainResource.Handler
{
    public static class ClientEvents
    {
        public static void DrawText(Client client, string text, bool draw)
        {
            client.TriggerEvent("Text:Draw", text, draw);
        }
    }
}
