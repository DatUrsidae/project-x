﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;

namespace MainResource.Handler {
	internal class Respawn : Script
    {
		internal static void PlayerJoined(Client client)
        {
		    client.TriggerEvent("RespawnCam");
            client.Position = new Vector3(0, 0, 0);
            client.TriggerEvent("ReSpawning");
        }

		internal static void SoundOnDeath(Client client)
        {
			client.TriggerEvent("SoundOnDeath");
		}

        internal static void Respawned(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            client.TriggerEvent("ReSpawned");
            player.PlayerInfo.Spawned = true;
            client.SetSharedData("Alive", true);
            client.TriggerEvent("DeleteJobs");
            if (player.Job != 0) player.Job = 0;
        }
	}
}