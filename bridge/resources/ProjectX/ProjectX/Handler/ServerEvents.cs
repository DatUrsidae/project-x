﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Teams;
using System;
using System.Threading.Tasks;

namespace MainResource.Handler
{
    internal class MainEvents : Script
    {
        [ServerEvent(Event.PlayerDisconnected)]
        public async void PlayerDisconnected(Client client, DisconnectionType type, string reason)
        {
            if (!client.GetPlayer(out var player))
            {
                Console.WriteLine("No player");
                return;
            }
            var handle = client.Handle.Value;

            if (player.Client.HasSharedData("Alive")) player.Client.ResetSharedData("Alive");
            if (player.Client.HasSharedData("BlipColor")) player.Client.ResetSharedData("BlipColor");

            player.Client.Name = player.PlayerInfo.PlayerName;

            if (player.Client.IsInVehicle)
            {
                player.PlayerInfo.Position = player.Client.Vehicle.Position;
            }
            else
            {
                player.PlayerInfo.Position = player.Client.Position;
            }
            await player.SaveAsync();

            Console.WriteLine($"{player.PlayerInfo.PlayerName} disconnected.");

            switch (type)
            {
                case DisconnectionType.Left:
                    NAPI.Notification.SendNotificationToAll($"~r~{client.Name}~w~ has quit the server.");
                    Console.WriteLine($"{client.Name} quit");
                    break;

                case DisconnectionType.Timeout:
                    NAPI.Notification.SendNotificationToAll($"~r~{client.Name}~w~ has timed out.");
                    Console.WriteLine($"{client.Name} timed out");
                    break;

                case DisconnectionType.Kicked:
                    NAPI.Notification.SendNotificationToAll($"~r~{client.Name}~w~ has been kicked for {reason}");
                    Console.WriteLine($"{client.Name} kicked");
                    break;
            }
            if (player.VehicleInfo.Vehicle != null)
            {
                NAPI.Task.Run(() =>
                {
                    player.VehicleInfo.Vehicle.Delete();
                });
            }
            PlayerData.Players.Remove(handle);
        }
    }

    internal abstract class ServerEvents : Script
    {
        [ServerEvent(Event.PlayerConnected)]
        public void PlayerConnected(Client client)
        {
            try
            {
                client.TriggerEvent("SpawnBase");
                OnPlayerConnect(client);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.ResourceStart)]
        public void ResourceStart()
        {
            try
            {
                OnResourceStart();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.PlayerEnterColshape)]
        public void PlayerEnterColshape(ColShape colShape, Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            player.ColShape = colShape;
            try
            {
                OnPlayerEnterColShape(colShape, player);
                if (player.Client.HasData("eDown:Action")) client.TriggerEvent("Text:Draw", "Press [E] to interact", true);
                else if (player.Client.HasData("eDown:UI")) client.TriggerEvent("Text:Draw", "Press [E] to interact", true);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.PlayerExitColshape)]
        public void PlayerExitColshape(ColShape colShape, Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.ColShape != null) player.ColShape = null;
            
            try
            {
                if (colShape.HasData("eDown:UI"))
                {
                    player.Client.TriggerEvent("Text:Draw", "Press [E] to interact", false);
                }
                if (player.Client.HasData("eDown:UI"))
                {
                    player.Client.TriggerEvent("Text:Draw", "Press [E] to interact", false);
                    player.Client.ResetData("eDown:UI");
                }
                if (player.Client.HasData("eDown:Action"))
                {
                    player.Client.TriggerEvent("Text:Draw", "fak", false);
                    player.Client.ResetData("eDown:Action");
                }
                OnPlayerExitColShape(colShape, player);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
        [ServerEvent(Event.PlayerExitVehicle)]
        public void PlayerExitVehicle(Client client, Vehicle veh)
        {
            if (!client.GetPlayer(out var player)) return;
            try
            {
                OnVehicleExit(veh, player);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.PlayerExitVehicleAttempt)]
        public void PlayerExitVehicleAttempt(Client client, Vehicle vehicle)
        {
            if (!client.GetPlayer(out var player)) return;
            try
            {
                OnVehicleExitAttempt(vehicle, player);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.PlayerEnterVehicleAttempt)]
        public void PlayerEnterVehicleAttempt(Client client, Vehicle veh, sbyte seatID)
        {
            if (!client.GetPlayer(out var player)) return;
            try
            {
                OnVehicleEnterAttempt(veh, seatID, player);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.PlayerDeath)]
        public void PlayerDeath(Client client, Client killer, uint reason)
        {
            if (!client.GetPlayer(out var player)) return;
            try
            {
                OnPlayerDeath(client, killer, reason);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [ServerEvent(Event.Update)]
        public void Update()
        {
            try
            {
                Tick();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        internal virtual void OnPlayerConnect(Client client) { }
        internal virtual void OnResourceStart() { }
        internal virtual void OnPlayerEnterColShape(ColShape colShape, Player player) { }
        internal virtual void OnPlayerExitColShape(ColShape colShape, Player player) { }
        internal virtual void OnVehicleExit(Vehicle vehicle, Player player) { }
        internal virtual void OnVehicleExitAttempt(Vehicle vehicle, Player player) { }
        internal virtual void OnVehicleEnterAttempt(Vehicle vehicle, sbyte seatID, Player player) { }
        internal virtual void OnPlayerDeath(Client client, Client killer, uint reason) { }
        internal virtual void Finished(Player player, bool completed, string reason = "") { }
        internal virtual void Rewards(Player player, bool completed) { }
        internal virtual void Start(ColShape colShape, Player player) { }
        internal virtual void Tick() { }
    }
}
