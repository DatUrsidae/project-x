﻿using MainResource.Classes.Missions;

namespace MainResource.Structs
{
    public struct MissionData
    {
        public bool InMission { get; set; }
        internal Mission Mission { get; set; }

        public void ResetData()
        {
            InMission = false;
            Mission = null;
        }
    }
}
