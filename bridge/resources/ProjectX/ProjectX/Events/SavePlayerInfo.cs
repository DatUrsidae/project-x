﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models.PlayerData;
using System;
using System.Timers;

namespace MainResource.Events
{
    internal class SavePlayerInfo : ServerEvents
    {
        internal override void OnResourceStart()
        {
            Timer timer = new Timer();
            timer.Interval = 1000;
            timer.Elapsed += (sender, e) => timerElapsed(sender, e);
            timer.Start();
        }

        internal async void timerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var players = NAPI.Pools.GetAllPlayers();
                if (players == null) return;
                foreach (var client in players)
                {
                    if (!client.GetPlayer(out var player)) return;
                    if (!player.PlayerInfo.Spawned) return;
                    //client.TriggerEvent("GetPlayerWeapons");
                    if (player.Client.IsInVehicle)
                    {
                        player.PlayerInfo.Position = player.Client.Vehicle.Position;
                    }
                    else
                    {
                        player.PlayerInfo.Position = player.Client.Position;
                    }
                    await player.SaveAsync();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
