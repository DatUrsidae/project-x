﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using System;
using MainResource.Events;
using MainResource.Teams;
using static MainResource.Utility.ChatColors;
using MainResource.Classes;
using MainResource.Enums;
using MainResource.Classes.Missions;
using MainResource.Structs;

namespace MainResource.Commands {

	public class Commands : Script
    {
        
        [Command("clothing")]
        internal void Clothing(Client client)
        {
            client.TriggerEvent("clothingShop:Open");
        }
        [Command("giveweapon")]
        internal void GiveWepon(Client client, WeaponHash weapon, int ammo)
        {
            client.TriggerEvent("GiveWeapon", weapon, ammo);
        }
        [Command("closedoors")]
        internal void CloseDoors(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            NAPI.ClientEvent.TriggerClientEventInRange(client.Position, 500, "CloseDoors", client.Value);
        }

        [Command("opendoor", "Usage: 0 - Front Left, 1 - Front Right, 2 - Back Left, 3 - Back Right, 4 - Hood, 5 - Trunk, 6 - Back, 7 - Back2")]
        internal void OpenDoor(Client client, int door)
        {
            if (!client.GetPlayer(out var player)) return;
            if (door > 7 || door < 0)
            {
                player.SendNotification("Error", "Use doors: 0-7", "warning");
                return;
            }
            if (player.Client.Vehicle != null)
            {
                NAPI.ClientEvent.TriggerClientEventInRange(client.Position, 500, "DoorControl", door, client.Value);
            }
        }
        [Command("addveh")]
        internal void AddCar(Client client, string veh, int team, int s)
        {
            if (!client.GetPlayer(out var player)) return;
            try
            {
                var hash = NAPI.Util.GetHashKey(veh);
                NAPI.Vehicle.CreateVehicle(hash, client.Position, client.Heading, 0, 0);
                new TeamVehicle(hash, (TeamEnum)team, (TeamSpecialization)s, client.Position, client.Heading);
                player.SendNotification("Team Vehicle", "Vehicle added succesfully", "success");
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [Command("spawncar")]
        internal void SpawnCar(Client client, string vehicle)
        {
            var car = NAPI.Util.GetHashKey(vehicle);
            var veh = NAPI.Vehicle.CreateVehicle(car, client.Position, client.Heading, 0, 0);
            client.SetIntoVehicle(veh, -1);
        }

        [Command("w", GreedyArg = true)]
        internal void Whisper(Client client, Client target, string message)
        {
            if (target == null)
            {
                client.SendNotification($"{target} is not online or could not be found");
                return;
            }
            if (target == client)
            {
                client.SendNotification($"Why would you whisper yourself?");
                return;
            }
            client.SendChatMessage($"{Purple}You whisper to {target.Name}: {message}");
            target.SendChatMessage($"{Purple}{client.Name} whispers to you: {message}");
        }

        [Command("stats")]
        internal void Stats(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.PlayerInfo.SelectedTeam == TeamEnum.None)
            {
                client.SendChatMessage($"[{Green}Personal{White}] {Orange}Name{White}: {player.PlayerInfo.PlayerName} - {Orange}EXP{White}: {player.PlayerInfo.EXP} - {Orange}Level{White}: {player.PlayerInfo.Level} - {Orange}Team{White}: N/A");
                return;
            }
            client.SendChatMessage($"[{Green}Personal{White}] {Orange}Name{White}: {player.PlayerInfo.PlayerName} - {Orange}EXP{White}: {player.PlayerInfo.EXP} - {Orange}Level{White}: {player.PlayerInfo.Level}");
            if (player.Team != null) client.SendChatMessage($"[{Green}Team{White}] {Orange}Team{White}: {player.Team.TeamName} - {Orange}EXP{White}: {player.Team.EXP} - {Orange}Level{White}: {player.Team.Level} - {Orange}Specialization{White}: {player.Team.Specialization.ToString()}");
        }

        /*[RemoteEvent("GotSpeed")]
        internal void GotSpeed(Client client, object[] args)
        {
            var speed = (float)args[0];
            int kmh = (int)Math.Round(speed);
            if (kmh >= 35)
            {
                client.SendNotification($"Broke window! You went {kmh} KM/H");
                client.TriggerEvent("StoreRob:Broke", player.Vehicle);
            }
            else
            {
                client.SendNotification($"Go faster! You went only {kmh} KM/H");
            }
        }*/
        [Command("jobcar")]
        internal void JobCar(Client client)
        {
            if (client.HasData("CarJacker:Car"))
            {
                var veh = (Vehicle)client.GetData("CarJacker:Car");
                client.SetIntoVehicle(veh, -1);
            }
        }
        [Command("givemoney")]
        internal void GiveMoney(Client client, uint amount)
        {
            if (amount > 0)
            {
                client.GetFromList().AddCredits(amount);
            }
        }

        [Command("ped")]
        internal void SpawnPed(Client client)
        {
            client.TriggerEvent("Ped");
        }

        [Command("neons")]
        internal void Neons(Client client)
        {
            client.Vehicle.Neons = true;
            client.Vehicle.NeonColor = new Color(RandomNumber(0, 255), RandomNumber(0, 255), RandomNumber(0, 255));
        }
        [Command("prison")]
        internal void Prison(Client client)
        {
            client.TriggerEvent("prison");
        }

        [Command("hello")]
        public void HELLONIGGA(Client client)
        {
            client.SendChatMessage($"Hello {client.SocialClubName}");
        }

        [Command("getstreet")]
        public void GetStreet(Client client)
        {
            client.TriggerEvent("GetStreet");
        }


        [Command("goto")]
        public void Goto(Client client, float x, float y, float z)
        {
            var position = new Vector3(x, y, z);
            client.Position = position;
        }

        [Command("createfaction")]
        public void Faction(Client client)
        {
            client.TriggerEvent("faction:Open");
        }
        
        [Command("kick")]
        public void Kick(Client client)
        {
            client.Kick();
        }
        [Command("clearwanted")]
        internal void CMD_ClearWanted(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            player.PlayerInfo.Wanted = 0;
            UpdateHUD.UpdateWanted(client.GetFromList().Client, 0);
        }

        [Command("teleport")]
        public void CMD_Teleport(Client client, Client target) {
            client.Position = target.Position;
        }


        [Command("dimension")]
        public void CMD_Dimension(Client client){
            client.SendChatMessage($"You are in dimension: ~y~{client.Dimension}");
        }

		[Command("tunecar")]
		public void CMD_TuneCar(Client client) {
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 3, 3);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 11, 3);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 0, 3);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 18, 0);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 15, 3);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 13, 2);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 12, 2);
			NAPI.Vehicle.SetVehicleMod(client.Vehicle, 40, 2);
            client.Vehicle.CustomPrimaryColor = new Color(RandomNumber(0, 255), RandomNumber(0, 255), RandomNumber(0, 255), RandomNumber(0, 255));
            client.Vehicle.CustomSecondaryColor = new Color(RandomNumber(0, 255), RandomNumber(0, 255), RandomNumber(0, 255), RandomNumber(0, 255));
        }

        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        [Command("respawn")]
		public void CMD_Respawn(Client client) {
			NAPI.Player.SetPlayerHealth(client, 0);
		}

		[Command("giveguns")]
		public void CMD_GiveGun(Client client) {
			foreach (WeaponHash weapon in Enum.GetValues(typeof(WeaponHash))) {
				NAPI.Player.GivePlayerWeapon(client, weapon, 100);
			}
		}

        [Command("pos")]
        public void GetPos(Client client)
        {
            Console.WriteLine(client.Position);
        }

		[Command("getpos")]
		public void CMD_GetPos(Client client) {
			Console.WriteLine($"new Vector3({client.Position.X}f, {client.Position.Y}f, {client.Position.Z}f)".ToString());
            Console.WriteLine($"{client.Rotation}");
		}
	}
}