﻿
namespace MainResource.Enums
{
    public enum TeamSpecialization
    {
        None = 0,
        Arms_Dealer = 1,
        Drug_Dealer = 2,
        Offense_Specialist = 3,
        Defense_Specialist = 4,
        Mechanic = 5,
        Trucker = 6,
        Medic = 7
    }
}
