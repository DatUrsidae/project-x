﻿namespace MainResource.Enums
{
    internal enum FactionRank
    {
        None = 0,
        Leader = 1,
        Member = 2
    }
}