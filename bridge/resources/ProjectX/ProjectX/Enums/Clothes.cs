﻿
namespace MainResource.Enums
{
    public enum CharacterClothes
    {
        FACE = 0,
        MASK = 1,
        HAIR = 2,
        TORSO = 3,
        LEGS = 4,
        BAGS = 5,
        FEET = 6,
        ACCESSORIES = 7,
        UNDERSHIRT = 8,
        BODY_ARMOR = 9,
        DECALS = 10,
        TOPS = 11
    }
}
