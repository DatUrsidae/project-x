﻿using GTANetworkAPI;
using MainResource.Handler;

namespace MainResource.Utility
{
    internal class RequestIpl : ServerEvents
    {
        internal override void OnResourceStart()
        {
            NAPI.World.RequestIpl("canyonrvrdeep");
            NAPI.World.RequestIpl("canyonrvrshallow");
            NAPI.World.RequestIpl("bkr_biker_interior_placement_interior_0_biker_dlc_int_01_milo"); //1107.04, -3157.399, -37.51859
            NAPI.World.RequestIpl("bkr_biker_interior_placement_interior_1_biker_dlc_int_02_milo"); //998.4809, -3164.711, -38.90733
            NAPI.World.RequestIpl("gr_case10_bunkerclosed"); //-3058.714, 3329.19, 12.5844
            NAPI.World.RequestIpl("gr_case9_bunkerclosed"); //24.43542, 2959.705, 58.35517
            NAPI.World.RequestIpl("gr_case3_bunkerclosed"); //481.0465, 2995.135, 43.96672
            NAPI.World.RequestIpl("gr_case0_bunkerclosed"); //848.6175, 2996.567, 45.81612
            NAPI.World.RequestIpl("gr_case1_bunkerclosed"); //2126.785, 3335.04, 48.21422
            NAPI.World.RequestIpl("gr_case2_bunkerclosed"); //2493.654, 3140.399, 51.28789
            NAPI.World.RequestIpl("gr_case5_bunkerclosed"); //1823.961, 4708.14, 42.4991
            NAPI.World.RequestIpl("gr_case7_bunkerclosed"); //-783.0755, 5934.686, 24.31475
            NAPI.World.RequestIpl("gr_case11_bunkerclosed"); //-3180.466, 1374.192, 19.9597
            NAPI.World.RequestIpl("gr_case6_bunkerclosed"); //1570.372, 2254.549, 78.89397
            NAPI.World.RequestIpl("gr_case4_bunkerclosed"); //-391.3216, 4363.728, 58.65862
            NAPI.World.RequestIpl("DT1_03_Shutter"); //23.9346, -669.7552, 30.8853
            NAPI.World.RequestIpl("facelobby"); //-1047.9, -233.0, 39.0
            NAPI.World.RequestIpl("SP1_10_real_interior"); //-248.6731, -2010.603, 30.14562
            NAPI.World.RequestIpl("post_hiest_unload"); //-630.07, -236.332, 38.05704
            NAPI.World.RequestIpl("refit_unload"); //-585.8247, -282.72, 35.45475
            NAPI.World.RequestIpl("csr_beforeMission"); //
            NAPI.World.RequestIpl("bh1_47_joshhse_unburnt"); //
            NAPI.World.RequestIpl("TownHall"); //269.93 -278.28 53.41 
            NAPI.World.RequestIpl("bkr_biker_interior_placement_interior_6_biker_dlc_int_ware05_milo"); // 1165, -3196.6, -39.01306
        }
    }
}
