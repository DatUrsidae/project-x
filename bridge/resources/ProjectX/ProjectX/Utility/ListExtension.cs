﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MainResource.Utility
{
    public static class ListExtension
    {
        public static List<T> AddMany<T>(this List<T> list, params T[] elements)
        {
            list.AddRange(elements);
            return list;
        }
    }
}
