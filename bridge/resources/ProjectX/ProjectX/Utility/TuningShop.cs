﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models;
using MainResource.Models.PlayerData;
using Newtonsoft.Json.Linq;
using System;
using static MainResource.Utility.ChatColors;

namespace MainResource.Utility
{
    internal class TuningShop : ServerEvents
    {
        internal TuningShop()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-326.4795f, -144.8621f, 39.05997f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(-326.4795f, -144.8621f, 39.05997f - 1), new Vector3(), new Vector3(), 3, new Color(255, 255, 255, 100));
            NAPI.Blip.CreateBlip(72, new Vector3(-326.4795f, -144.8621f, 39.05997f), 0.6f, 3, "Tuning Shop", 255, 50, true);
            colShape.SetData("TuningShop", null);
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("TuningShop") && player.Client.IsInVehicle)
            {
                player.Client.TriggerEvent("tuning:OpenContainer");
                player.Client.TriggerEvent("FreezePlayer");
                //var playerVehicle = player.Vehicle;
                var dimension = (uint)player.Client.Value + 1;
                player.Client.Dimension = dimension;
                player.Client.Vehicle.Dimension = dimension;
                player.Client.TriggerEvent("Tuning", player.Client.Vehicle, player.Client.Vehicle.CustomPrimaryColor.Red, player.Client.Vehicle.CustomPrimaryColor.Green, player.Client.Vehicle.CustomPrimaryColor.Blue, player.Client.Vehicle.CustomSecondaryColor.Red, player.Client.Vehicle.CustomSecondaryColor.Green, player.Client.Vehicle.CustomSecondaryColor.Blue);
            }
        }

        [RemoteEvent("tuningPurchase")]
        public void OnTuningPurchase(Client client, object[] args)
        {
            var player = client.GetFromList();
            var modType = (int)args[0];
            var modIndex = (int)args[1];
            var price = (int)args[2];
            var modName = (string)args[3];
            var wheelType = (int)args[4];
            var wheelIndex = (int)args[5];


            if (player.PlayerInfo.Credits < price)
            {
                client.SendNotification($"{Red}You don't have enough money with you to buy this modification!");
                return;
            }
            if (player.Client.Vehicle.GetMod(modType) == modIndex)
            {
                client.SendNotification("You already have this mod bought!");
                return;
            }

            player.PlayerInfo.Credits -= Convert.ToUInt32(price);
            client.SendNotification($"You purchased {modName} for {price}$");
            if (modType == 46)
            {
                player.Client.Vehicle.WindowTint = modIndex;
                client.TriggerEvent("tuning:PlaySound");
                return;
            }

            if (modType == 23)
            {
                player.Client.Vehicle.WheelType = wheelType;
                player.Client.Vehicle.SetMod(modType, wheelIndex);
                client.TriggerEvent("tuning:PlaySound");
                return;
            }
            player.Client.Vehicle.SetMod(modType, modIndex);
            client.TriggerEvent("tuning:PlaySound");
        }
        [RemoteEvent("colorPrimaryPurchase")]
        public void OnPrimaryColorPurchase(Client client, object[] args)
        {
            JObject o = JObject.Parse((string)args[0]);
            client.Vehicle.CustomPrimaryColor = new Color((int)o.Property("r").Value, (int)o.Property("g").Value, (int)o.Property("b").Value);
        }
        [RemoteEvent("colorSecondaryPurchase")]
        public void OnSecondaryColorPurchase(Client client, object[] args)
        {
            JObject o = JObject.Parse((string)args[0]);
            client.Vehicle.CustomSecondaryColor = new Color((int)o.Property("r").Value, (int)o.Property("g").Value, (int)o.Property("b").Value);
        }

        [RemoteEvent("tuningClose")]
        public void OnTuningClose(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player))
            client.Dimension = 0;
            client.Vehicle.Dimension = 0;
            if (player.VehicleInfo.Vehicle != null)
            {
                player.PlayerInfo.UpdatePlayerVehicle(player.VehicleInfo.VehicleID, player.VehicleInfo.Vehicle);
            }
            
            client.TriggerEvent("UnFreezePlayer");
        }
    }
}
