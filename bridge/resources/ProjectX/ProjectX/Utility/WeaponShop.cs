﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Events;
using System;
using MainResource.Handler;

namespace MainResource.Utility
{
    internal class WeaponShop : ServerEvents
    {
        internal WeaponShop()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(21.84971f, -1107.524f, 29.79703f), 1, 1);
            NAPI.Marker.CreateMarker(1, new Vector3(21.84971f, -1107.524f, 29.79703f -1), new Vector3(), new Vector3(), 2, new Color(200, 0, 0, 100));
            NAPI.Blip.CreateBlip(110, new Vector3(21.84971f, -1107.524f, 29.79703f), 0.6f, 76, "Weapon Shop", 255, 50, true);
            colShape.SetData("eDown:UI", "weaponShop:Open");
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("eDown:UI") && !player.Client.IsInVehicle)
            {
                player.Client.SetData("eDown:UI", colShape.GetData("eDown:UI"));
            }
        }

        [RemoteEvent("OnWeaponPurchaseAttempt")]
        internal void OnWeaponPurchaseAttempt(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            var hash = (string)args[0];
            var weaponHash = Convert.ToUInt32(hash, 16);
            var weaponName = (string)args[1];
            var price = (int)args[2];


            if (player.PlayerInfo.Credits < price)
            {
                client.SendChatMessage($"~r~You don't have enough credits with you to buy ~y~{weaponName}~r~.");
                return;
            }

            player.PlayerInfo.Credits -= Convert.ToUInt32(price);
            player.GiveWeapon((WeaponHash)weaponHash, 100);
            player.SendNotification("Weapon Shop", $"{weaponName} has been purchased for {price} credits", "success");
            UpdateHUD.UpdateCredits(player.Client, player.PlayerInfo.Credits);
        }
    }
}
