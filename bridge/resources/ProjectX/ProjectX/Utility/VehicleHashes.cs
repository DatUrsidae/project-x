﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace MainResource.Utility
{
    static class VehicleHashes
    {
        static List<VehicleHash> hashes { get; set; } = new List<VehicleHash>();

        static VehicleHashes()
        {
            hashes.Add(VehicleHash.Adder);
            hashes.Add(VehicleHash.Akuma);
            hashes.Add(VehicleHash.Alpha);
            hashes.Add(VehicleHash.Asea);
            hashes.Add(VehicleHash.AUTARCH);
            hashes.Add(VehicleHash.Banshee);
            hashes.Add(VehicleHash.Banshee2);
            hashes.Add(VehicleHash.Bati);
            hashes.Add(VehicleHash.BestiaGTS);
            hashes.Add(VehicleHash.BF400);
            hashes.Add(VehicleHash.BfInjection);
            hashes.Add(VehicleHash.Bison);
            hashes.Add(VehicleHash.Blade);
            hashes.Add(VehicleHash.Blazer);
            hashes.Add(VehicleHash.Blista);
            hashes.Add(VehicleHash.Blista2);
            hashes.Add(VehicleHash.BobcatXL);
            hashes.Add(VehicleHash.Boxville);
            hashes.Add(VehicleHash.BType);
        }

        internal static List<VehicleHash> GetHashes()
        {
            return hashes;
        }
    }
}
