using ProjectX_Client.CEF;
using RAGE;
using System;

namespace Main.CEF
{
    public class HUDWindow : HtmlWindow
    {
        public HUDWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = false;
            Route = "Bank";
            WindowPath = "package://ServerUI/index.html#/HUD";
            CreateNew();
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            Events.Add("setHudBalance", (object[] args) => {
                CefWindow.ExecuteJs($"{EventBus.Send}('setHudBalance', '{args[0]}');");
            });
            Events.Add("setHudWanted", (object[] args) => {
                CefWindow.ExecuteJs($"{EventBus.Send}('setHudWanted', '{args[0]}');");
            });
            Events.Add("notification", (object[] args) =>
            {
                var title = (string)args[0];
                var content = (string)args[1];
                var variant = (string)args[2];
                CefWindow.ExecuteJs($"{EventBus.Send}('makeNotification', '{title}', '{content}', '{variant}')");
                RAGE.Game.Audio.PlaySoundFrontend(1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET", true);
            });
        }
    }
}
