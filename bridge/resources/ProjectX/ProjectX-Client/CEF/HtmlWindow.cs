using RAGE;
using System.Threading.Tasks;

namespace Main.CEF {
    public abstract class HtmlWindow {
        public string WindowPath { get; set; }
        public string Route { get; set; }
        public RAGE.Ui.HtmlWindow CefWindow { get; set; }
        public bool IsOpen { get; private set; }
        public bool CloseWhenOtherRenders { get; set; } = true;
        public bool LockCursorWhenOpen { get; set; }
        public void CreateNew() {
            Events.OnBrowserDomReady += OnBrowserDomReady;
            Events.Add("cef:HideOther", (object[] args) => {
                if (!CloseWhenOtherRenders)
                    return;
                IsOpen = false;
                CefWindow.ExecuteJs("openUrl('Home')");
            });
            CefWindow = new RAGE.Ui.HtmlWindow(WindowPath);
            RegisterEvents();
        }
        void OnBrowserDomReady(RAGE.Ui.HtmlWindow htmlWindow) {
            if (htmlWindow == CefWindow)
                OnWindowReady();
        }
        public abstract void RegisterEvents();
        public abstract void OnWindowReady();

        public void ToggleWindowVisibility() {
            if (!IsOpen)
                RenderWindow();
            else
                HideWindow();
        }
        public virtual void RenderWindow() {
            IsOpen = true;
            CefWindow.ExecuteJs($"openUrl('{Route}')");
            Events.CallLocal("cef:HideOther");
            Task.Delay(250).ContinueWith((task) => { RAGE.Ui.Cursor.Visible = true; });
        }
        public virtual void HideWindow() {
            RAGE.Ui.Cursor.Visible = false;
            IsOpen = false;
            CefWindow.ExecuteJs("openUrl('Home')");
        }
    }
}
