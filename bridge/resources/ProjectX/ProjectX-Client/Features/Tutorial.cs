﻿using RAGE;
using Main;
using System.Collections.Generic;

namespace ProjectX_Client.Features
{
    class Tutorial : Events.Script
    {
        internal Tutorial()
        {
            Events.Add("Tutorial:Start", (object[] args) =>
            {
                RAGE.Game.Ui.SetNewWaypoint(233.1486f, -410.4735f);
                Events.Tick += OnTick;
                Events.CallLocal("notification", "Tutorial", "Hey, welcome to the server. I have placed a route for you to get you started, see you there.", "warning");
            });
        }

        internal void OnTick(List<Events.TickNametagData> nametags)
        {
            if (RAGE.Elements.Player.LocalPlayer.Position.DistanceTo(new Vector3(233.1486f, -410.4735f, 48.11198f)) < 35f)
            {
                Events.CallLocal("notification", "Tutorial", "Great, you found it. Now go inside the Town Hall and get yourself signed up for Civilians", "success");
                RAGE.Game.Ui.RefreshWaypoint();
                Events.Tick -= OnTick;
            }
        }
    }
}
