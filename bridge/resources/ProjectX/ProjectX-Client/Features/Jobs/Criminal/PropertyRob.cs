﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RAGE;
using RAGE.Elements;

namespace ProjectX_Client.Features.Jobs.Criminal
{
    internal class PropertyRobSpawns
    {
        internal Vector3 ColShapeVector { get; set; }
        internal Marker Marker { get; set; }
        static List<PropertyRobSpawns> PropertyRobs = new List<PropertyRobSpawns>();

        internal PropertyRobSpawns(Vector3 colShapeVector)
        {
            ColShapeVector = colShapeVector;
            Marker = new Marker(28, colShapeVector.Subtract(new Vector3(0, 0, 1)), 2f, new Vector3(), new Vector3(), new RGBA(255, 255, 255, 25));
            PropertyRobs.Add(this);
        }

        internal static void DeleteStoreRobSpawns()
        {
            if (PropertyRobs.Count != 0)
            {
                foreach (PropertyRobSpawns propertyRobSpawn in PropertyRobs)
                {
                    propertyRobSpawn.Marker.Destroy();
                }
            }
        }
    }

    internal class Loot
    {
        internal string Name { get; private set; } = null;
        internal string PropName { get; private set; } = null;
        internal MapObject Object { get; set; } = null;
        internal uint Worth { get; set; } = 0;

        internal Loot SpawnLoot(Vector3 vector, int r)
        {
            switch (r)
            {
                case 0:
                    Name = "Ammo Crate";
                    PropName = "ex_prop_adv_case_sm_flash";
                    Worth = 300;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                case 1:
                    Name = "Supply Crate";
                    PropName = "ex_prop_adv_case_sm_03";
                    Worth = 100;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                case 2:
                    Name = "Trash Bag";
                    PropName = "ng_proc_binbag_01a";
                    Worth = 2;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                case 3:
                    Name = "Cardboard Box";
                    PropName = "prop_cardbordbox_04a";
                    Worth = 15;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                case 4:
                    Name = "Television";
                    PropName = "prop_cctv_01_sm_02";
                    Worth = 150;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                case 5:
                    Name = "Beer Crate";
                    PropName = "prop_crate_11e";
                    Worth = 55;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                case 6:
                    Name = "PC Monitor";
                    PropName = "prop_crt_mon_01";
                    Worth = 100;
                    Object = new MapObject(RAGE.Game.Misc.GetHashKey(PropName), vector, new Vector3());
                    return this;
                default:
                    return null;
            }
        }

        internal void DestroyObject()
        {
            if (Object != null)
            {
                Object.Destroy();
            }
        }
    }
    internal class PropertyRob : Events.Script
    {
        List<Loot> Pickups { get; set; } = new List<Loot>();
        internal Loot Pickup { get; set; } = null;
        List<Vector3> ObjectSpawns { get; set; } = null;
        bool Carrying { get; set; } = false;
        MapObject CarryObject { get; set; } = null;
        Vehicle Vehicle { get; set; } = null;
        Marker Marker { get; set; } = null;
        Blip Blip { get; set; } = null;
        bool KeyDown { get; set; } = false;
        internal PropertyRob()
        {
            Events.Add("Criminal", (object[] args) =>
            {
                //Sound Sanctuary
                new PropertyRobSpawns(new Vector3(386.9616f, -969.8793f, 29.44273f));
                //Wholesome Mart
                new PropertyRobSpawns(new Vector3(371.4796f, -941.7711f, 29.43681f));
                //Bean Machine Coffee
                new PropertyRobSpawns(new Vector3(125.3785f, -1027.928f, 29.35735f));
                //Ground & Pound
                new PropertyRobSpawns(new Vector3(54.87864f, -799.486f, 31.58714f));
                //Yogarishima
                new PropertyRobSpawns(new Vector3(-521.2753f, -855.6256f, 30.24436f));
                //Wook Noodle House
                new PropertyRobSpawns(new Vector3(-655.9249f, -880.1505f, 24.6949f));
                //Pops Pills
                new PropertyRobSpawns(new Vector3(103.2609f, -2.272199f, 68.00032f));
                //Hawaiian Snow
                new PropertyRobSpawns(new Vector3(253.1523f, -205.8862f, 54.11035f));
                //Porncrackers
                new PropertyRobSpawns(new Vector3(219.1441f, -1531.811f, 29.2913f));
                //Greenback
                new PropertyRobSpawns(new Vector3(-602.1436f, -1125.011f, 22.32425f));
            });

            Events.Add("DeleteJobs", (object[] args) =>
            {
                PropertyRobSpawns.DeleteStoreRobSpawns();
            });

            Events.Add("PropertyRob:Broke", (object[] args) =>
            {
                if (Carrying)
                {
                    DeleteObjects();
                    CarryObject.Destroy();
                    Carrying = false;
                    Events.CallRemote("CarryAnim", false);
                }
                if (Pickups.Count != 0) DeleteObjects();
                Vehicle = (Vehicle)args[0];
                var j = (JArray)args[1];
                ObjectSpawns = j.ToObject<List<Vector3>>();
                Random r = new Random();
                for(int i = r.Next(0, ObjectSpawns.Count); i < ObjectSpawns.Count; i++)
                {
                    var o = new Loot().SpawnLoot(ObjectSpawns[i], r.Next(0, 7));
                    o.Object.PlaceOnGroundProperly();
                    o.Object.SetCollision(false, false);
                    Pickups.Add(o);
                }
                Events.Tick += PickUpObject;
            });
            Events.Add("BreakWindowSound", (object[] args) =>
            {
                RAGE.Game.Audio.RequestScriptAudioBank("ARM_3_03", false, -1);
                RAGE.Game.Audio.RequestScriptAudioBank("ARM_3_02_CAR_CRASH", false, -1);
                RAGE.Game.Audio.RequestScriptAudioBank("ARM_3_01", false, -1);
                RAGE.Game.Audio.PlaySoundFrontend(-1, "ARM_3_CAR_GLASS_CRASH", "0", true);
                RAGE.Game.Audio.ReleaseScriptAudioBank();
            });

            Events.Add("PropertyRob:DropOff", (object[] args) =>
            {
                Marker = new Marker(1, new Vector3(893.436f, -889.5112f, 27.05505f - 1f), 3, new Vector3(), new Vector3(), new RGBA(200, 0, 0, 150));
                Blip = new Blip(478, new Vector3(893.436f, -889.5112f, 27.05505f), "Loot Drop-off", 0.8f, 2);
            });

            Events.Add("PropertyRob:DroppedOff", (object[] args) =>
            {
                Marker.Destroy();
                Blip.Destroy();
            });

            Events.Add("PropertyRob:DeleteObjects", (object[] args) =>
            {
                DeleteObjects();
            });
        }

        internal void DeleteObjects()
        {
            Events.Tick -= PickUpObject;
            if (Pickups.Count == 0) return;
            foreach (var pickup in Pickups.ToList())
            {
                pickup.Object.Destroy();
                Pickups.Remove(pickup);
            }
        }

        internal void PickUpObject(List<Events.TickNametagData> nametags)
        {
            if (Pickups.Count > 0)
            {
                foreach (var pickup in Pickups)
                {
                    
                    if (Input.IsDown(69) && RAGE.Game.Entity.GetEntityCoords(pickup.Object.Handle, false).DistanceTo(Player.LocalPlayer.Position) < 1.5f && !Carrying && !Player.LocalPlayer.IsInAnyVehicle(false) && !KeyDown)
                    {
                        KeyDown = true;
                        if (Player.LocalPlayer.GetSelectedWeapon() != 0xA2719263) Player.LocalPlayer.SetCurrentWeapon(0xA2719263, true);
                        CarryObject = pickup.Object;
                        Pickup = pickup;
                        Carrying = true;
                        RAGE.Game.Entity.AttachEntityToEntity(pickup.Object.Handle, Player.LocalPlayer.Handle, Player.LocalPlayer.GetBoneIndex(4090), 0.2f, 0.2f, 0.2f, 0f, -70f, -30f, false, false, false, false, 2, true);
                        Events.CallRemote("CarryAnim", true);
                        Task.Delay(500).ContinueWith((task) => { KeyDown = false; });
                    }

                    if (Player.LocalPlayer.Position.DistanceTo(pickup.Object.Position) > 100f)
                    {
                        DeleteObjects();
                        Events.CallRemote("Property:Clear");
                        Events.CallRemote("CarryAnim", false);
                    }
                }

                if (Input.IsDown(69) && Carrying && CarryObject != null && !KeyDown)
                {
                    KeyDown = true;
                    RAGE.Game.Entity.DetachEntity(CarryObject.Handle, true, false);
                    CarryObject.PlaceOnGroundProperly();
                    CarryObject.SetCollision(false, false);
                    Carrying = false;
                    CarryObject = null;
                    Events.CallRemote("CarryAnim", false);
                    Task.Delay(500).ContinueWith((task) => { KeyDown = false; });
                }

                
            }

            if (Carrying && Vehicle.Position.DistanceTo(Player.LocalPlayer.Position) < 2f && CarryObject != null)
            {
                CarryObject.Destroy();
                Carrying = false;
                Events.CallRemote("AddLoot", Pickup.Name, Pickup.Worth);
                Pickups.Remove(Pickup);
                CarryObject = null;
                Events.CallRemote("CarryAnim", false);
            }

            if (Pickups.Count == 0)
            {
                Events.Tick -= PickUpObject;
                Events.CallRemote("Property:Clear");
            }
        }
    }
}
