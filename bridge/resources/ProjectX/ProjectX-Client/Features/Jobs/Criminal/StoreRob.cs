using RAGE;
using RAGE.Elements;
using System.Collections.Generic;

namespace Main.Features.Jobs {
    internal class StoreRobSpawns
    {
        internal Vector3 colShapeVector;
        internal Marker marker;
        static List<StoreRobSpawns> storeRobs = new List<StoreRobSpawns>();

        internal StoreRobSpawns(Vector3 colShapeVector)
        {
            this.colShapeVector = colShapeVector;
            marker = new Marker(1, colShapeVector.Subtract(new Vector3(0,0,1)), 1, new Vector3(), new Vector3(), new RGBA(200, 0, 0, 100));
            storeRobs.Add(this);
        }

        internal static void DeleteHouseRobSpawns()
        {
            if (storeRobs.Count != 0)
            {
                foreach (StoreRobSpawns houserobSpawns in storeRobs)
                {
                    houserobSpawns.marker.Destroy();
                }
            }
        }
    }

    internal class HouseRob : Events.Script
    {

        internal HouseRob()
        {
            Events.Add("Criminal", (object[] args) =>
            {
                new StoreRobSpawns(new Vector3(-1487.657f, -379.314f, 40.16343f));
                new StoreRobSpawns(new Vector3(-2968.353f, 391.0988f, 15.04331f));
                new StoreRobSpawns(new Vector3(26.38923f, -1347.312f, 29.49703f));
                new StoreRobSpawns(new Vector3(-1821.483f, 793.0137f, 138.124f));
                new StoreRobSpawns(new Vector3(1162.889f, -323.0853f, 69.20508f));
                new StoreRobSpawns(new Vector3(1982.573f, 3053.242f, 47.21507f));
                new StoreRobSpawns(new Vector3(1699.148f, 4924.482f, 42.06364f));
                new StoreRobSpawns(new Vector3(1729.372f, 6414.442f, 35.03722f));
                new StoreRobSpawns(new Vector3(547.4503f, 2671.18f, 42.1565f));
                new StoreRobSpawns(new Vector3(1165.989f, 2708.864f, 38.1577f));
            });
            Events.Add("DeleteJobs", (object[] args) =>
            {
                StoreRobSpawns.DeleteHouseRobSpawns();
            });
        }
    }
}
