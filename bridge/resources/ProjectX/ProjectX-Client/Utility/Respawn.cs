using RAGE;
using System.Collections.Generic;
using System.Drawing;

namespace Main
{
    public class Respawn : Events.Script
    {
        static int ResX = 0;
        static int ResY = 0;
        static int respawnCam;
        static string text;
        public Respawn()
        {
            
            Events.Add("Text:Draw", (object[] args) =>
            {
                text = (string)args[0];
                var draw = (bool)args[1];
                RAGE.Game.Graphics.GetScreenResolution(ref ResX, ref ResY);

                if (draw)
                {
                    Events.Tick += DrawText;
                }
                else if (!draw)
                {
                    text = null;
                    Events.Tick -= DrawText;
                }

            });
            Events.Add("RespawnCam", RespawnCamera);
            Events.Add("ReSpawning", ReSpawning);
            Events.Add("ReSpawned", ReSpawned);
        }

        private static void DrawText(List<Events.TickNametagData> nametags)
        {
            RAGE.Game.UIText.Draw($"{text}", new Point(ResX - ResX / 2, ResY - ResY / 2 - 250), 1f, Color.White, RAGE.Game.Font.ChaletComprimeCologne, true);
        }
        
        private static void RespawnCamera(object[] args)
        {
            RAGE.Game.Cam.DestroyAllCams(false);
            respawnCam = RAGE.Game.Cam.CreateCameraWithParams(RAGE.Game.Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), -397.9332f, 1095.172f, 364.947f, 0, 0, 0, 70, false, 2);
            RAGE.Game.Cam.SetCamActive(respawnCam, true);
            RAGE.Game.Cam.RenderScriptCams(true, true, 500, false, false, 0);
        }

        private static void ReSpawning(object[] args)
        {
            RAGE.Elements.Player.LocalPlayer.SetAlpha(0, false);
            RAGE.Elements.Player.LocalPlayer.FreezePosition(true);
        }

        private static void ReSpawned(object[] args)
        {
            RAGE.Elements.Player.LocalPlayer.SetAlpha(255, false);
            RAGE.Elements.Player.LocalPlayer.FreezePosition(false);
            RAGE.Game.Cam.RenderScriptCams(false, true, 2000, false, false, 0);
            RAGE.Game.Cam.DestroyCam(respawnCam, true);
        }
    }
}
