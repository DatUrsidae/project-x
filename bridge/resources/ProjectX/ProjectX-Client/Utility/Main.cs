using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using RAGE;
using RAGE.Elements;

namespace Main {
    public static class Notifications
    {
        public static void DrawNotification(string message)
        {
            RAGE.Game.Ui.SetNotificationTextEntry("STRING");
            RAGE.Game.Ui.AddTextComponentSubstringPlayerName(message);
            RAGE.Game.Ui.DrawNotification(false, false);
        }
    }
    internal class DPedStat
    {
        public static string Stamina = "SP0_STAMINA";
        public static string Shooting = "SP0_SHOOTING_ABILITY";
        public static string Strength = "SP0_STRENGTH";
        public static string Stealth = "SP0_STEALTH_ABILITY";
        public static string Flying = "SP0_FLYING_ABILITY";
        public static string Wheelie = "SP0_WHEELIE_ABILITY";
        public static string Lung = "SP0_LUNG_CAPACITY";
    }
    public class Main : Events.Script
    {
        static bool keyDown = false;
        static bool eDown = false;
        static int ResX = 0;
        static int ResY = 0;
        public Main()
        {
            Events.Add("SpawnBase", (object[] args) =>
            {
                foreach(var ped in Entities.Peds.All)
                {
                    ped.Destroy();
                }
                var blip = new Blip(411, new Vector3(233.1486f, -410.4735f, 48.11198f), "Town Hall", 0.8f, 4, 255, 50, true);
                new Ped(0x13AEF042, new Vector3(372.4194f, 326.5573f, 103.5664f), 263.5592f);
            });
            Events.Add("GetForwardVector", (object[] args) =>
            {
                var position = Player.LocalPlayer.Position + Player.LocalPlayer.GetForwardVector();
                Events.CallRemote("GotForwardVector", position.X, position.Y, position.Z);
            });
            Events.Add("CloseDoors", (object[] args) =>
            {
                var id = (int)args[0];
                var client = Entities.Players.All.Find(a => a.RemoteId == id);
                if (client.IsInAnyVehicle(false))
                {
                    client.Vehicle.SetDoorsShut(true);
                }
            });
            Events.Add("DoorControl", (object[] args) =>
            {
                var door = (int)args[0];
                var id = (int)args[1];
                var client = Entities.Players.All.Find(a => a.RemoteId == id);

                if (client.IsInAnyVehicle(false))
                {
                    if (client.Vehicle.IsDoorFullyOpen(door))
                    {
                        client.Vehicle.SetDoorShut(door, true);
                    }
                    else if (!client.Vehicle.IsDoorFullyOpen(door))
                    {
                        client.Vehicle.SetDoorOpen(door, true, true);
                    }
                }
            });
            Events.Add("PlaySound", PlaySound);
			Events.Add("SoundOnDeath", SoundOnDeath);
            Events.Add("FreezePlayer", FreezePlayer);
            Events.Add("UnFreezePlayer", UnFreezePlayer);
            Events.Add("GetPlayerWeapons", GetPlayerWeapons);
            Events.Add("CleanBlips", CleanBlips);
            //Events.Add("GetSpeed", GetSpeed);
            Events.Add("Notification", (object[] args) =>
            {
                string message = (string)args[0];
                Notifications.DrawNotification(message);
            });
            Events.Add("Invincible", (object[] args) =>
            {
                var yn = (bool)args[0];
                
                if (yn)
                {
                    Player.LocalPlayer.SetInvincible(true);
                    Player.LocalPlayer.SetCanRagdoll(false);
                    var height = Player.LocalPlayer.GetHeightAboveGround();
                    if (height <= 2f) height = 0;
                    Events.CallRemote("SetPlayerToGround", height);
                }
                else if (!yn)
                {
                    Player.LocalPlayer.SetInvincible(false);
                    Player.LocalPlayer.SetCanRagdoll(true);
                }
                //Player.LocalPlayer.Position.Subtract(new Vector3(0, 0, height));
            });
            


            Chat.Colors = true;
            Nametags.Enabled = false;
            Events.Tick += OnUpdate;
            RAGE.Game.Graphics.GetScreenResolution(ref ResX, ref ResY);

            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Flying), 100, false);
            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Lung), 100, false);
            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Shooting), 100, false);
            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Stamina), 100, false);
            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Stealth), 100, false);
            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Strength), 100, false);
            RAGE.Game.Stats.StatSetInt(RAGE.Game.Misc.GetHashKey(DPedStat.Wheelie), 100, false);

            RAGE.Game.Audio.SetAudioFlag("LoadMPData", true);
            RAGE.Game.Audio.SetAudioFlag("DisableFlightMusic", true);
            

            StreetNames.StreetNamesEvent();
            DisableAmbience();
            OpenDoors();
            CloseDoors();
            AddInteriorProps();
        }

        internal static int GetSpeed()
        {
            var veh = Player.LocalPlayer.Vehicle;
            var speed = veh.GetSpeed();
            var kmh = (float)(speed * 3.6);
            var toSingle = (int)Math.Round(kmh);
            return toSingle;
            //Events.CallRemote("GotSpeed", kmh);
        }

		private static void OnUpdate(List<Events.TickNametagData> nametags)
        {
            var players = Entities.Players.All;
            if (Player.LocalPlayer.IsInAnyVehicle(false))
            {
                RAGE.Game.UIText.Draw($"{GetSpeed()} KMH", new Point(ResX - ResX / 2 - 430, ResY - ResY / 2 + 326), 0.5f, Color.White, RAGE.Game.Font.Monospace, false);
            }
            foreach (var player in players)
            {
                if (Player.LocalPlayer.Position.DistanceTo2D(player.Position) < 20f && player.GetSharedData("Alive") != null && player.GetBlipFrom() == 0 && player.Handle != Player.LocalPlayer.Handle)
                {
                    if ((bool)player.GetSharedData("Alive") && player.GetSharedData("BlipColor") != null)
                    {
                        var blipColor = (int)player.GetSharedData("BlipColor");
                        var blip = RAGE.Game.Ui.AddBlipForEntity(player.Handle);
                        RAGE.Game.Ui.SetBlipSprite(blip, 1);
                        RAGE.Game.Ui.SetBlipColour(blip, blipColor);
                        RAGE.Game.Ui.SetBlipScale(blip, 0.5f);
                    }
                }
                if (Player.LocalPlayer.Position.DistanceTo2D(player.Position) > 21f)
                {
                    var blip = player.GetBlipFrom();
                    RAGE.Game.Ui.RemoveBlip(ref blip);
                }
                if (player.GetSharedData("Alive") != null)
                {
                    if (!(bool)player.GetSharedData("Alive"))
                    {
                        var blip = player.GetBlipFrom();
                        RAGE.Game.Ui.RemoveBlip(ref blip);
                    }
                }
                if (RAGE.Game.Entity.HasEntityBeenDamagedByEntity(Player.LocalPlayer.Handle, player.Handle, true)) {
                    if (player.Handle == Player.LocalPlayer.Handle) {
                        RAGE.Game.Entity.ClearEntityLastDamageEntity(Player.LocalPlayer.Handle);
                        return;
                    }
                    Events.CallRemote("Damaged", player.RemoteId, Player.LocalPlayer.RemoteId);
                    Task.Delay(200).ContinueWith((task) => { RAGE.Game.Entity.ClearEntityLastDamageEntity(Player.LocalPlayer.Handle); });
                }
            }

            if (Input.IsDown(113) && !keyDown)
            {
                if (RAGE.Ui.Cursor.Visible == true)
                {
                    RAGE.Ui.Cursor.Visible = false;
                }
                else
                {
                    RAGE.Ui.Cursor.Visible = true;
                }
                keyDown = true;
                
                Task.Delay(500).ContinueWith((task) => { keyDown = false; });
            }

            if (Input.IsDown(89) && !keyDown)
            {
                keyDown = true;
                Task.Delay(500).ContinueWith((task) => { keyDown = false; });
            }

            if (Input.IsDown(69) && !eDown)
            {
                Events.CallRemote("eDown");
                eDown = true;
                Task.Delay(500).ContinueWith((task) => { eDown = false; });
            }

            if (Input.IsDown(76) && !keyDown)
            {
                Events.CallRemote("lDown");

                keyDown = true;
                Task.Delay(500).ContinueWith((task) => { keyDown = false; });
            }
            
		}

        private static void CleanBlips(object[] args)
        {
            foreach(Blip blip in Entities.Blips.All)
            {
                blip.Destroy();
            }
        }

		private static void PlaySound(object[] args)
        {
            var soundName = (string)args[0];
            var soundSet = (string)args[1];
            RAGE.Game.Audio.PlaySoundFrontend(1, soundName, soundSet, true);
        }

		private static void SoundOnDeath(object[] args)
        {
			RAGE.Game.Audio.PlaySoundFrontend(1, "Bed", "WastedSounds", true);
		}

        private static void FreezePlayer(object[] args)
        {
            Player.LocalPlayer.FreezePosition(true);
            if (Player.LocalPlayer.IsSittingInAnyVehicle())
            {
                Player.LocalPlayer.Vehicle.FreezePosition(true);
            }
        }

        private static void UnFreezePlayer(object[] args)
        {
            Player.LocalPlayer.FreezePosition(false);
            if (Player.LocalPlayer.IsSittingInAnyVehicle())
            {
                Player.LocalPlayer.Vehicle.FreezePosition(false);
            }
        }

        private static void GetPlayerWeapons(object[] args)
        {
            string[] hashes = {
                "0x92A27487",
                "0x92A2748",
                "0x958A4A8F",
                "0xF9E6AA4B",
                "0x84BD7BFD",
                "0x8BB05FD7",
                "0x440E4788",
                "0x4E875F73",
                "0xF9DCBF2D",
                "0xD8DF3C3C",
                "0x99B507EA",
                "0xDD5DF8D9",
                "0xDFE37640",
                "0x678B81B1",
                "0x19044EE0",
                "0xCD274149",
                "0x94117305",
                "0x3813FC08",
                "0x1B06D571",
                "0xBFE256D4",
                "0x5EF9FEC4",
                "0x22D8FE39",
                "0x3656C8C1",
                "0x99AEEB3B",
                "0xBFD21232",
                "0x88374054",
                "0xD205520E",
                "0x83839C4",
                "0x47757124",
                "0xDC4DB296",
                "0xC1B3C3D1",
                "0xCB96392F",
                "0x97EA20B8",
                "0xAF3696A1",
                "0x13532244",
                "0x2BE6766B",
                "0x78A97CD0",
                "0xEFE7E2DF",
                "0xA3D4D34",
                "0xDB1AA450",
                "0xBD248B55",
                "0x476BF155",
                "0x1D073A89",
                "0x555AF99A",
                "0x7846A318",
                "0xE284C527",
                "0x9D61E50F",
                "0xA89CB99E",
                "0x3AABBBAA",
                "0xEF951FBB",
                "0x12E82D3D",
                "0xBFEFFF6D",
                "0x394F415C",
                "0x83BF0278",
                "0xFAD1F1C9",
                "0xAF113F99",
                "0xC0A3098D",
                "0x969C3D67",
                "0x7F229F94",
                "0x84D6FAFD",
                "0x624FE830",
                "0x9D07F764",
                "0x7FD62962",
                "0xDBBD7280",
                "0x61012683",
                "0x5FC3C11",
                "0xC472FE2",
                "0xA914799",
                "0xC734385A",
                "0x6A6C02E0",
                "0xB1CA77B1",
                "0xA284510B",
                "0x4DD2DC56",
                "0x42BF8A85",
                "0x7F7497E5",
                "0x6D544C99",
                "0x63AB0442",
                "0x781FE4A",
                "0xB62D1F67",
                "0x93E220BD",
                "0xA0973D5E",
                "0xFDBC8A50",
                "0x497FACC3",
                "0x24B17070",
                "0x2C3731D9",
                "0xAB564B93",
                "0x787F0BB",
                "0xBA45E8B8",
                "0x23C9F95C",
                "0x34A67B97",
                "0x60EC506",
                "0xFBAB5776"};

            Dictionary<uint, int> playerWeapons = new Dictionary<uint, int>();
            foreach(string hash in hashes)
            {
                var realHash = Convert.ToUInt32(hash, 16);
                var weapon = Player.LocalPlayer.HasGotWeapon(realHash, false);
                
                if (weapon)
                {
                    var ammo = Player.LocalPlayer.GetAmmoInWeapon(realHash);
                    playerWeapons.Add(realHash, ammo);
                }
            }

            Events.CallRemote("GotPlayerWeapons", playerWeapons);
        }

        private static void OpenDoors()
        {
            //Mission Row Police Station Armory
            RAGE.Game.Object.DoorControl(749848321, 453.0793f, -983.1895f, 30.83926f, true, 0, 0, 0);
            //Discount Store South Enter Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1148826190), 82.38156f, -1390.476f, 29.52609f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(868499217, 82.38156f, -1390.752f, 29.52609f, false, 0, 0, 0);
            //Los Santos Customs Popular Street Door
            RAGE.Game.Object.DoorControl(270330101, 723.116f, -1088.831f, 23.23201f, false, 0, 0, 0);
            //Los Santos Customs Carcer Way Door	
            RAGE.Game.Object.DoorControl(unchecked((uint)-550347177), -356.0905f, -134.7714f, 40.01295f, false, 0, 0, 0);
            //Los Santos Customs Greenwich Parkway Door	
            RAGE.Game.Object.DoorControl(unchecked((uint)-550347177), -1145.898f, -1991.144f, 14.18357f, false, 0, 0, 0);
            //Los Santos Customs Route 68 Doors
            RAGE.Game.Object.DoorControl(unchecked((uint)-822900180), 1174.656f, 2644.159f, 40.50673f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(unchecked((uint)-822900180), 1182.307f, 2644.166f, 40.50784f, false, 0, 0, 0);
            //Beeker's Garage Paleto Bay Doors
            RAGE.Game.Object.DoorControl(unchecked((uint)-822900180), 114.3135f, 6623.233f, 32.67305f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(unchecked((uint)-822900180), 108.8502f, 6617.877f, 32.67305f, false, 0, 0, 0);
            //Ammu Nation Popular Street Doors 
            RAGE.Game.Object.DoorControl(unchecked((uint)-8873588), 18.572f, -1115.495f, 29.94694f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(97297972, 16.12787f, -1114.606f, 29.94694f, false, 0, 0, 0);
            //Ammu Nation Vespucci Boulevard Doors 
            RAGE.Game.Object.DoorControl(unchecked((uint)-8873588), 842.7685f, -1024.539f, 28.34478f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(97297972, 845.3694f, -1024.539f, 28.34478f, false, 0, 0, 0);
            //Ammu Nation Lindsay Circus Doors 
            RAGE.Game.Object.DoorControl(unchecked((uint)-8873588), -662.6415f, -944.3256f, 21.97915f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(97297972, -665.2424f, -944.3256f, 21.97915f, false, 0, 0, 0);
            //Ammu Nation Popular Street Doors 
            RAGE.Game.Object.DoorControl(unchecked((uint)-8873588), 810.5769f, -2148.27f, 29.76892f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(97297972, 813.1779f, -2148.27f, 29.76892f, false, 0, 0, 0);
            //Ammu Nation Vinewood Plaza Doors
            RAGE.Game.Object.DoorControl(unchecked((uint)-8873588), 243.8379f, -46.52324f, 70.09098f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(97297972, 244.7275f, -44.07911f, 70.09098f, false, 0, 0, 0);
            //Ponsonbys Portola Drive Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1922281023), -715.6154f, -157.2561f, 37.67493f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(unchecked((uint)-1922281023), -716.6755f, -155.42f, 37.67493f, false, 0, 0, 0);
            //Ponsonbys Portola Drive Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1922281023), -1456.201f, -233.3682f, 50.05648f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(unchecked((uint)-1922281023), -1454.782f, -231.7927f, 50.05649f, false, 0, 0, 0);
            //Ponsonbys Rockford Plaza Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1922281023), -156.439f, -304.4294f, 39.99308f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(unchecked((uint)-1922281023), -157.1293f, -306.4341f, 39.99308f, false, 0, 0, 0);
            //Sub Urban Prosperity Street Promenade Door
            RAGE.Game.Object.DoorControl(1780022985, -1201.435f, -776.8566f, 17.99184f, false, 0, 0, 0);
            //Sub Urban Hawick Avenue Door
            RAGE.Game.Object.DoorControl(1780022985, 127.8201f, -211.8274f, 55.22751f, false, 0, 0, 0);
            //Sub Urban Route 68 Door
            RAGE.Game.Object.DoorControl(1780022985, 617.2458f, 2751.022f, 42.75777f, false, 0, 0, 0);
            //Sub Urban Chumash Plaza Door
            RAGE.Game.Object.DoorControl(1780022985, -3167.75f, 1055.536f, 21.53288f, false, 0, 0, 0);
            //Bob Mulét Barber Shop Door
            RAGE.Game.Object.DoorControl(145369505, -822.4442f, -188.3924f, 37.81895f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(unchecked((uint)-1663512092), -823.2001f, -187.0831f, 37.81895f, false, 0, 0, 0);
            //Hair on Hawick Barber Shop Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1844444717), -29.86917f, -148.1571f, 57.22648f, false, 0, 0, 0);
            //O'Sheas Barber Shop Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1844444717), 1932.952f, 3725.154f, 32.9944f, false, 0, 0, 0);
            //Premium Deluxe Motorsport Parking Doors
            RAGE.Game.Object.DoorControl(1417577297, -37.33113f, -1108.873f, 26.7198f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(2059227086, -39.13366f, -1108.218f, 26.7198f, false, 0, 0, 0);
            //Premium Deluxe Motorsport Main Doors
            RAGE.Game.Object.DoorControl(1417577297, -60.54582f, -1094.749f, 26.88872f, false, 0, 0, 0);
            RAGE.Game.Object.DoorControl(2059227086, -59.89302f, -1092.952f, 26.88362f, false, 0, 0, 0);
            //Vanilla Unicorn Main Enter Door
            RAGE.Game.Object.DoorControl(unchecked((uint)-1116041313), 127.9552f, -1298.503f, 29.41962f, false, 0, 0, 0);
        }

        private static void CloseDoors()
        {
            //Bolingbroke Penitentiary Main Enter First Door
            RAGE.Game.Object.DoorControl(741314661, 1844.998f, 2597.482f, 44.63626f, true, 0, 0, 0);
            //Bolingbroke Penitentiary Main Enter Second Door
            RAGE.Game.Object.DoorControl(741314661, 1818.543f, 2597.482f, 44.60749f, true, 0, 0, 0);
            //Bolingbroke Penitentiary Main Enter Third Door
            RAGE.Game.Object.DoorControl(741314661, 1806.939f, 2616.975f, 44.60093f, true, 0, 0, 0);
        }

        private static void AddInteriorProps()
        {
            RAGE.Game.Interior.EnableInteriorProp(7170, "csr_beforeMission");
        }

        private static void DisableAmbience() {
            //Disable ambience sounds
            RAGE.Game.Audio.StartAudioScene("FBI_HEIST_H5_MUTE_AMBIENCE_SCENE");
            RAGE.Game.Invoker.Invoke(0xB4F90FAF7670B16F, false); //police reports
            RAGE.Game.Invoker.Invoke(0x218DD44AAAC964FF, "AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL", true, 0);
            RAGE.Game.Invoker.Invoke(0x218DD44AAAC964FF, "AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_WARNING", true, 0);
            RAGE.Game.Invoker.Invoke(0x218DD44AAAC964FF, "AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM", true, 0);
            RAGE.Game.Invoker.Invoke(0xBDA07E5950085E46, 0, false, false);
            RAGE.Game.Invoker.Invoke(0x1D6650420CEC9D3B, "AZ_DISTANT_SASQUATCH", 0, 0);
        }
	}
}
