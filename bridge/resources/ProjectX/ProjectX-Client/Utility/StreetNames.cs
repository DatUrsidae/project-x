using RAGE;
using RAGE.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Main
{
    class StreetNames
    {
        internal static void StreetNamesEvent()
        {
            Events.Add("GetStreet", Streets);
        }
       
        internal static void Streets(object[] args)
        {
            int streetNameValue = 0;
            int crossingPoint = 0;
            string zoneId = Zone.GetNameOfZone(RAGE.Elements.Player.LocalPlayer.Position.X, RAGE.Elements.Player.LocalPlayer.Position.Y,
            RAGE.Elements.Player.LocalPlayer.Position.Z);

            Pathfind.GetStreetNameAtCoord(RAGE.Elements.Player.LocalPlayer.Position.X, RAGE.Elements.Player.LocalPlayer.Position.Y, RAGE.Elements.Player.LocalPlayer.Position.Z, ref streetNameValue, ref crossingPoint);

            string streetName = Ui.GetStreetNameFromHashKey((uint)streetNameValue);

            string zoneNameShort =
                "AIRP, ALAMO, ALTA, ARMYB, BANHAMC, BANNING, BEACH, BHAMCA, BRADP, BRADT, BURTON, CALAFB, CANNY, CCREAK, CHAMH, CHIL, CHU, CMSW, CYPRE, DAVIS, DELBE, DELPE, DELSOL, DESRT, DOWNT, DTVINE, EAST_V, EBURO, ELGORL, ELYSIAN, GALFISH, GOLF, GRAPES, GREATC, HARMO, HAWICK, HORS, HUMLAB, JAIL, KOREAT, LACT, LAGO, LDAM, LEGSQU, LMESA, LOSPUER, MIRR, MORN, MOVIE, MTCHIL, MTGORDO, MTJOSE, MURRI, NCHU, NOOSE, OCEANA, PALCOV, PALETO, PALFOR, PALHIGH, PALMPOW, PBLUFF, PBOX, PROCOB, RANCHO, RGLEN, RICHM, ROCKF, RTRAK, SANAND, SANCHIA, SANDY, SKID, SLAB, STAD, STRAW, TATAMO, TERMINA, TEXTI, TONGVAH, TONGVAV, VCANA, VESP, VINE, WINDF, WVINE, ZANCUDO, ZP_ORT, ZQ_UAR";

            string[] zoneShortParse = zoneNameShort.Split(", ").ToArray();

            string zoneListComp =
                "Los Santos International Airport, Alamo Sea, Alta, Fort Zancudo, Banham Canyon Dr, Banning, Vespucci Beach, Banham Canyon, Braddock Pass, Braddock Tunnel, Burton, Calafia Bridge, Raton Canyon, Cassidy Creek, Chamberlain Hills, Vinewood Hills, Chumash, Chiliad Mountain State Wilderness, Cypress Flats, Davis, Del Perro Beach, Del Perro, La Puerta, Grand Senora Desert, Downtown, Downtown Vinewood, East Vinewood, El Burro Heights, El Gordo Lighthouse, Elysian Island, Galilee, GWC and Golfing Society, Grapeseed, Great Chaparral, Harmony, Hawick, Vinewood Racetrack, Humane Labs and Research, Bolingbroke Penitentiary, Little Seoul, Land Act Reservoir, Lago Zancudo, Land Act Dam, Legion Square, La Mesa, La Puerta, Mirror Park, Morningwood, Richards Majestic, Mount Chiliad, Mount Gordo, Mount Josiah, Murrieta Heights, North Chumash, N.O.O.S.E, Pacific Ocean, Paleto Cove, Paleto Bay, Paleto Forest, Palomino Highlands, Palmer - Taylor Power Station, Pacific Bluffs, Pillbox Hill, Procopio Beach, Rancho, Richman Glen, Richman, Rockford Hills, Redwood Lights Track, San Andreas, San Chianski Mountain Range, Sandy Shores, Mission Row, Stab City, Maze Bank Arena, Strawberry, Tataviam Mountains, Terminal, Textile City, Tongva Hills, Tongva Valley, Vespucci Canals, Vespucci, Vinewood, Ron Alternates Wind Farm, West Vinewood, Zancudo River, Port of South Los Santos, Davis Quartz";

            string[] zoneListParse = zoneListComp.Split(", ");

            string zoneName = null;

            if (zoneShortParse.Contains(zoneId))
            {
                int zId = Array.IndexOf(zoneShortParse, zoneId);

                zoneName = zoneListParse[zId];
            }
            if (zoneName != null)
            {
                Chat.Output($"Street: {streetName} Zone: {zoneName}");
                Events.CallRemote("Streets", streetName, zoneName);
                return;
            }
            else if (zoneName == null)
            {
                zoneName = "N/A";
                Chat.Output($"Street: {streetName} Zone: {zoneName}");
                Events.CallRemote("Streets", streetName, zoneName);
                return;
            }
        }
    }      
}
