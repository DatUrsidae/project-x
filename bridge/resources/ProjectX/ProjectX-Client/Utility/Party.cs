﻿using System.Collections.Generic;
using System.Drawing;
using RAGE;

namespace ProjectX_Client.Utility
{
    public class Party : Events.Script
    {
        public string Player1 = "";
        public string Player2 = "";
        public string Player3 = "";
        public string Player4 = "";

        static int ResX = 0;
        static int ResY = 0;
        static int Members = 0;
        public Party()
        {
            Events.Add("Party:Draw", (object[] args) =>
            {
                switch (args.Length)
                {
                    case 1:
                        Player1 = (string)args[0];
                        Members = 1;
                        break;
                    case 2:
                        Player1 = (string)args[0];
                        Player2 = (string)args[1];
                        Members = 2;
                        break;
                    case 3:
                        Player1 = (string)args[0];
                        Player2 = (string)args[1];
                        Player3 = (string)args[2];
                        Members = 3;
                        break;
                    case 4:
                        Player1 = (string)args[0];
                        Player2 = (string)args[1];
                        Player3 = (string)args[2];
                        Player4 = (string)args[3];
                        Members = 4;
                        break;
                    default:
                        break;
                }
                Events.Tick += DrawParty;
            });
            Events.Add("Party:StopDraw", (object[] args) =>
            {
                Events.Tick -= DrawParty;
                Player1 = "";
                Player2 = "";
                Player3 = "";
                Player4 = "";
            });

            RAGE.Game.Graphics.GetScreenResolution(ref ResX, ref ResY);
        }

        public void DrawParty(List<Events.TickNametagData> nametags)
        {
            RAGE.Game.UIText.Draw($"Party ({Members.ToString()} / 4)", new Point(ResX - ResX / 2 + 300, ResY - ResY / 2 + 100), 0.5f, Color.White, RAGE.Game.Font.ChaletComprimeCologne, true);
            RAGE.Game.UIText.Draw($"{Player1}", new Point(ResX - ResX / 2 + 300, ResY - ResY / 2 + 120), 0.5f, Color.White, RAGE.Game.Font.ChaletComprimeCologne, true);
            RAGE.Game.UIText.Draw($"{Player2}", new Point(ResX - ResX / 2 + 300, ResY - ResY / 2 + 130), 0.5f, Color.White, RAGE.Game.Font.ChaletComprimeCologne, true);
            RAGE.Game.UIText.Draw($"{Player3}", new Point(ResX - ResX / 2 + 300, ResY - ResY / 2 + 140), 0.5f, Color.White, RAGE.Game.Font.ChaletComprimeCologne, true);
            RAGE.Game.UIText.Draw($"{Player4}", new Point(ResX - ResX / 2 + 300, ResY - ResY / 2 + 150), 0.5f, Color.White, RAGE.Game.Font.ChaletComprimeCologne, true);
        }
    }
}
