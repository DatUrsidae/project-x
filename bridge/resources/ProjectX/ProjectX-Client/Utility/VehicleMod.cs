using Newtonsoft.Json;
using System;
using Newtonsoft.Json.Linq;
using RAGE.Elements;
using RAGE;
using System.Collections.Generic;

namespace Main
{
    public class VehicleMod
    {
        public string Title { get; set; }
        public int Value { get; set; }
        public Dictionary<string, int> Items { get; set; } = new Dictionary<string, int>();

        public VehicleMod(Vehicle vehicle, string title, string itemName, int value)
        {
            if (value == 50)
            {
                Title = "Neon";
                Value = 50;
                Items.Add($"{itemName} Stock", -1);
                Items.Add("Full Neon", 0);
                return;
            }
            if (value == 18)
            {
                Title = "Turbo";
                Value = 18;
                Items.Add($"{itemName} Stock", -1);
                Items.Add($"Turbo Upgrade", 0);
                return;
            }

            if (vehicle.GetNumMods(value) == 0) return;
            Title = title;
            Value = value;
            Items.Add($"{itemName} Stock", -1);

            for (var i = 0; i < vehicle.GetNumMods(value); i++)
            {
                Items.Add($"{itemName} - {i}", i);
            }
        }

        internal static List<VehicleMod> Mods(Vehicle vehicle)
        {
            List<VehicleMod> mods = new List<VehicleMod>();
            mods.Add(new VehicleMod(vehicle, "Spoilers", "Spoiler", 0));
            mods.Add(new VehicleMod(vehicle, "Front Bumpers", "Front Bumper", 1));
            mods.Add(new VehicleMod(vehicle, "Rear Bumpers", "Rear Bumper", 2));
            mods.Add(new VehicleMod(vehicle, "Side Skirts", "Side Skirt", 3));
            mods.Add(new VehicleMod(vehicle, "Exhausts", "Exhaust", 4));
            mods.Add(new VehicleMod(vehicle, "Frames", "Frame", 5));
            mods.Add(new VehicleMod(vehicle, "Grille", "Grille", 6));
            mods.Add(new VehicleMod(vehicle, "Hoods", "Hood", 7));
            mods.Add(new VehicleMod(vehicle, "Fenders", "Fender", 8));
            mods.Add(new VehicleMod(vehicle, "Right Fenders", "Right Fender", 9));
            mods.Add(new VehicleMod(vehicle, "Roofs", "Roof", 10));
            mods.Add(new VehicleMod(vehicle, "Engines", "Engine", 11));
            mods.Add(new VehicleMod(vehicle, "Brakes", "Brake", 12));
            mods.Add(new VehicleMod(vehicle, "Transmissions", "Transmission", 13));
            mods.Add(new VehicleMod(vehicle, "Horns", "Horn", 14));
            mods.Add(new VehicleMod(vehicle, "Suspension", "Suspension", 15));
            mods.Add(new VehicleMod(vehicle, "Armor", "Armor", 16));
            mods.Add(new VehicleMod(vehicle, "UNK17", "UNK17", 17));
            mods.Add(new VehicleMod(vehicle, "Turbos", "Turbo", 18));
            mods.Add(new VehicleMod(vehicle, "UNK19", "UNK19", 19));
            mods.Add(new VehicleMod(vehicle, "Tire Smokes", "Tire Smoke", 20));
            mods.Add(new VehicleMod(vehicle, "UNK21", "UNK21", 21));
            mods.Add(new VehicleMod(vehicle, "Xenon", "Xenon", 22));
            mods.Add(new VehicleMod(vehicle, "Back Wheels", "Back Wheel", 24));
            mods.Add(new VehicleMod(vehicle, "Plate Holders", "Plate Holder", 25));
            mods.Add(new VehicleMod(vehicle, "Vanity Plates", "Vanity Plate", 26));
            mods.Add(new VehicleMod(vehicle, "Trim Designs", "Trim Design", 27));
            mods.Add(new VehicleMod(vehicle, "Ornaments", "Ornament", 28));
            mods.Add(new VehicleMod(vehicle, "Dashboards", "Dashboard", 29));
            mods.Add(new VehicleMod(vehicle, "Dial Designs", "Dial Design", 30));
            mods.Add(new VehicleMod(vehicle, "Door Speakers", "Door Speaker", 31));
            mods.Add(new VehicleMod(vehicle, "Seats", "Seat", 32));
            mods.Add(new VehicleMod(vehicle, "Steering Wheels", "Steering Wheel", 33));
            mods.Add(new VehicleMod(vehicle, "Shift Levers", "Shift Lever", 34));
            mods.Add(new VehicleMod(vehicle, "Plaques", "Plaque", 35));
            mods.Add(new VehicleMod(vehicle, "Speakers", "Speaker", 36));
            mods.Add(new VehicleMod(vehicle, "UNK37", "UNK37", 37));
            mods.Add(new VehicleMod(vehicle, "Hydraulics", "Hydraulic", 38));
            mods.Add(new VehicleMod(vehicle, "Engine Blocks", "Engine Block", 39));
            mods.Add(new VehicleMod(vehicle, "Boosts", "Boost", 40));
            mods.Add(new VehicleMod(vehicle, "Struts", "Strut", 41));
            mods.Add(new VehicleMod(vehicle, "Arch Covers", "Arch Cover", 42));
            mods.Add(new VehicleMod(vehicle, "Aerials", "Aerial", 43));
            mods.Add(new VehicleMod(vehicle, "Trims", "Trim", 44));
            mods.Add(new VehicleMod(vehicle, "Tanks", "Tank", 45));
            mods.Add(new VehicleMod(vehicle, "Window Tints", "Window Tint", 46));
            mods.Add(new VehicleMod(vehicle, "UNK47", "UNK47", 47));
            mods.Add(new VehicleMod(vehicle, "Liveries", "Livery", 48));
            mods.Add(new VehicleMod(vehicle, "UNK49", "UNK49", 49));
            mods.Add(new VehicleMod(vehicle, "Neons", "Neon", 50));

            return mods;
        }

        internal static List<VehicleMod> Wheels(Vehicle vehicle)
        {
            List<VehicleMod> Wheels = new List<VehicleMod>();
            vehicle.SetWheelType(0);
            Wheels.Add(new VehicleMod(vehicle, "Sports Wheels", "Sport Wheel", 23));
            vehicle.SetWheelType(1);
            Wheels.Add(new VehicleMod(vehicle, "Muscle Wheels", "Muscle Wheel", 23));
            vehicle.SetWheelType(2);
            Wheels.Add(new VehicleMod(vehicle, "Lowrider Wheels", "Lowrider Wheel", 23));
            vehicle.SetWheelType(3);
            Wheels.Add(new VehicleMod(vehicle, "SUV Wheels", "SUV Wheel", 23));
            vehicle.SetWheelType(4);
            Wheels.Add(new VehicleMod(vehicle, "Offroad Wheels", "Offroad Wheel", 23));
            vehicle.SetWheelType(5);
            Wheels.Add(new VehicleMod(vehicle, "Tuner Wheels", "Tuner Wheel", 23));
            vehicle.SetWheelType(6);
            Wheels.Add(new VehicleMod(vehicle, "Bike Wheels", "Bike Wheel", 23));
            vehicle.SetWheelType(7);
            Wheels.Add(new VehicleMod(vehicle, "High End Wheels", "High End Wheel", 23));
            vehicle.SetWheelType(8);
            Wheels.Add(new VehicleMod(vehicle, "Benny's Original Wheels", "Benny's Original Wheel", 23));
            vehicle.SetWheelType(9);
            Wheels.Add(new VehicleMod(vehicle, "Benny's Bespoke Wheels", "Benny's Bespoke Wheel", 23));

            return Wheels;
        }
    }
}

        
    

