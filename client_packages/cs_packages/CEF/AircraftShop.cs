using RAGE;
using RAGE.Elements;
using System;

namespace Main.CEF
{
    public class AircraftShopWindow : HtmlWindow
    {
        public AircraftShopWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "AircraftShop";
            WindowPath = "package://ServerUI/index.html#/AircraftShop";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        int shopCam;
        Vehicle vehicle { get; set; } = null;
        float initRota = 65f;
        int price;
        string name;
        int hash;
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("aircraftShop:Close", (object[] args) => {
                initRota = 65f;
                if (vehicle != null) vehicle.Destroy();
                vehicle = null;
                Events.CallRemote("AircraftShopClose");
                HideWindow();
                RAGE.Game.Cam.RenderScriptCams(false, true, 1000, false, false, 0);
                RAGE.Game.Cam.DestroyCam(shopCam, true);
            });
            Events.Add("aircraftShop:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("AircraftShopOpen");
                Player.LocalPlayer.FreezePosition(true);
                shopCam = RAGE.Game.Cam.CreateCameraWithParams(RAGE.Game.Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), -992.1653f, -3029.314f, 25f, 0, 0, 326.8211f, 70, false, 2);
                RAGE.Game.Cam.SetCamActive(shopCam, true);
                RAGE.Game.Cam.RenderScriptCams(true, true, 500, false, false, 0);
            });
            Events.Add("aircraftShop:selection", (object[] args) => {
                initRota = 65f;
                hash = (int)args[0];
                price = (int)args[1];
                name = (string)args[2];

                if (vehicle != null)
                {
                    vehicle.Destroy();
                }
                vehicle = new Vehicle((uint)hash, new Vector3(-981.25f, -3005.864f, 13.94548f));
                vehicle.SetHeading(initRota);
            });
            Events.Add("aircraftShop:purchase", (object[] args) => {
                if (vehicle == null)
                {
                    Chat.Output("No vehicle selected to buy!");
                    return;
                }

                Events.CallRemote("AircraftPurchaseAttempt", hash, price, name, vehicle);
            });
            Events.Add("aircraftShop:RotatePlus", (object[] args) =>
            {
                initRota -= 40f;
                vehicle.SetHeading(initRota);
            });
            Events.Add("aircraftShop:RotatePlus", (object[] args) =>
            {
                initRota += 40f;
                vehicle.SetHeading(initRota);
            });
        }
    }
}
