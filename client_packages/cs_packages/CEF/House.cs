using RAGE;
using System;

namespace Main.CEF
{
    public class HouseWindow : HtmlWindow
    {
        public HouseWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "House";
            WindowPath = "package://ServerUI/index.html#/House";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            Events.Add("house:Close", (object[] args) => {
                HideWindow();
            });
            Events.Add("house:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("GetHouse");
            });
            Events.Add("house:Enter", (object[] args) =>
            {
                Events.CallRemote("GetInHouse");
            });
            Events.Add("house:getHouse", (object[] args) => {
                CefWindow.ExecuteJs($"EventBus.$emit('getHouse', '{(string)args[0]}', '{(string)args[1]}', '{(int)args[2]}', '{(string)args[3]}', '{(string)args[4]}', '{(string)args[5]}');");
            });
            Events.Add("house:BuyHouse", (object[] args) =>
            {
                Events.CallRemote("BuyHouse", args[0], args[1]);
            });
            Events.Add("house:SellHouse", (object[] args) =>
            {
                Events.CallRemote("SellHouse", args[0]);
            });
        }
    }
}
