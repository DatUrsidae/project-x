using RAGE;
using RAGE.Elements;
using System;

namespace Main.CEF {
    public class VehicleShopWindow : HtmlWindow {
        public VehicleShopWindow() {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "VehicleShop";
            WindowPath = "package://ServerUI/index.html#/VehicleShop";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        int shopCam;
        Vehicle vehicle { get; set; } = null;
        float initRota = 65f;
        int price;
        string name;
        int hash;
        public override void RegisterEvents() {
            // Server -> Client -> CEF requests
            Events.Add("vehicleShop:Close", (object[] args) => {
                initRota = 65f;
                if (vehicle!= null) vehicle.Destroy();
                vehicle = null;
                Events.CallRemote("VehicleShopClose");
                Player.LocalPlayer.FreezePosition(false);
                HideWindow();
                RAGE.Game.Cam.RenderScriptCams(false, true, 1000, false, false, 0);
                RAGE.Game.Cam.DestroyCam(shopCam, true);
            });
            Events.Add("vehicleShop:Open", (object[] args) => {
                RenderWindow();
                RAGE.Game.Cam.DestroyAllCams(true);
                Events.CallRemote("VehicleShopOpen");
                Player.LocalPlayer.FreezePosition(true);
                shopCam = RAGE.Game.Cam.CreateCameraWithParams(RAGE.Game.Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), -43.69959f, -1101.123f, 27.0f, 0, 0, 0, 70, false, 2);
                RAGE.Game.Cam.SetCamActive(shopCam, true);
                RAGE.Game.Cam.RenderScriptCams(true, true, 500, false, false, 0);
            });
            Events.Add("vehicleShop:selection", (object[] args) => {
                initRota = 65f;
                hash = (int)args[0];
                price = (int)args[1];
                name = (string)args[2];
                
                if (vehicle != null)
                {
                    vehicle.Destroy();
                }
                vehicle = new Vehicle((uint)hash, new Vector3(-43.46873f, -1095.643f, 26.42235f - 1));
                vehicle.SetHeading(initRota);
            });
            Events.Add("vehicleShop:purchase", (object[] args) => {
                if (vehicle == null)
                {
                    Chat.Output("No vehicle selected to buy!");
                    return;
                }

                Events.CallRemote("VehiclePurchaseAttempt", hash, price, name, vehicle);
            });
            Events.Add("vehicleShop:RotatePlus", (object[] args) =>
            {
                initRota -= 40f;
                vehicle.SetHeading(initRota);
            });
            Events.Add("vehicleShop:RotateMinus", (object[] args) =>
            {
                initRota += 40f;
                vehicle.SetHeading(initRota);
            });
        }
    }
}
