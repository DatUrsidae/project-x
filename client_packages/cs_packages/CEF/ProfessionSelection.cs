using RAGE;

namespace Main.CEF
{
    public class ProfessionSelectionWindow : HtmlWindow
    {

        public ProfessionSelectionWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "ProfessionSelection";
            WindowPath = "package://ServerUI/index.html#/ProfessionSelection";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("ProfessionSelection:Close", (object[] args) =>
            {
                HideWindow();
                Chat.Activate(true);
            });
            Events.Add("ProfessionSelection:Open", (object[] args) =>
            {
                RenderWindow();
                Chat.Activate(false);
            });
            Events.Add("selectJob", (object[] args) =>
            {
                Events.CallRemote("PlayerSelectJob", args[0]);
            });
        }
    }
}
