using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RAGE;
using RAGE.Elements;

namespace Main.CEF
{
    public class TuningWindow : HtmlWindow
    {
        //TODO ADD DIFFERENT PAINTJOBS
        public TuningWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            WindowPath = "package://ServerUI/index.html#/TuningShop";
            Route = "TuningShop";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        int tuningCam;
        int modType;
        int modIndex;
        string modName;
        int price;
        int wheelType;
        int wheelIndex;
        Vehicle vehicle { get; set; } = null;
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("tuning:CloseContainer", (object[] args) => {
                if (vehicle.Exists) vehicle.Destroy();
                vehicle = null;
                RAGE.Game.Cam.RenderScriptCams(false, true, 1000, false, false, 0);
                RAGE.Game.Cam.DestroyAllCams(true);
                RAGE.Game.Audio.ReleaseScriptAudioBank();
                Events.CallRemote("tuningClose");
                HideWindow();
                Player.LocalPlayer.Vehicle.SetVisible(true, false);
            });
            Events.Add("tuning:OpenContainer", (object[] args) => {
                RenderWindow();
                RAGE.Game.Audio.RequestScriptAudioBank("DLC_JA16/MOD_SHOP_SULTANRS", false, -1);
                RAGE.Game.Audio.RequestScriptAudioBank("VEHICLE_SHOP_HUD_1", false, -1);
                RAGE.Game.Audio.RequestScriptAudioBank("VEHICLE_SHOP_HUD_2", false, -1);
                tuningCam = RAGE.Game.Cam.CreateCameraWithParams(RAGE.Game.Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), -343.8575f, -137.1127f, 39.5f, 0, 0, 270f, 70, false, 2);
                RAGE.Game.Cam.SetCamActive(tuningCam, true);
                RAGE.Game.Cam.RenderScriptCams(true, true, 500, false, false, 0);
                vehicle.SetModColor1(1, 2, 0);
            });
            Events.Add("tuning:modSelection", (object[] args) =>
            {
                modType = (int)args[0];
                modIndex = (int)args[1];
                modName = (string)args[2];


                if (modType == 50)
                {
                    vehicle.SetNeonLightEnabled(2, true);
                    vehicle.SetNeonLightEnabled(3, true);
                    vehicle.SetNeonLightEnabled(0, true);
                    vehicle.SetNeonLightEnabled(1, true);
                    vehicle.SetNeonLightsColour(255, 0, 0);
                    return;
                }
                vehicle.SetMod(modType, modIndex, false);
            });
            Events.Add("tuning:wheelSelection", (object[] args) =>
            {
                modType = (int)args[0];
                wheelIndex = (int)args[1];
                wheelType = (int)args[2];
                modName = (string)args[3];
                vehicle.SetWheelType(wheelType);
                vehicle.SetMod(23, wheelIndex, false);
            });
            Events.Add("tuning:primaryColor", (object[] args) =>
            {
                var colors = (string)args[0];
                JObject o = JObject.Parse(colors);
                vehicle.SetCustomPrimaryColour((int)o.Property("r").Value, (int)o.Property("g").Value, (int)o.Property("b").Value);
            });
            Events.Add("tuning:secondaryColor", (object[] args) =>
            {
                var colors = (string)args[0];
                JObject o = JObject.Parse(colors);
                vehicle.SetCustomSecondaryColour((int)o.Property("r").Value, (int)o.Property("g").Value, (int)o.Property("b").Value);
            });
            Events.Add("tuning:wheelColor", (object[] args) =>
            {
                
            });
            Events.Add("tuning:neonColor", (object[] args) =>
            {
                var colors = (string)args[0];
                JObject o = JObject.Parse(colors);
                RAGE.Game.Invoker.Invoke(RAGE.Game.Natives.SetVehicleNeonLightsColour, vehicle, ((int)o.Property("r").Value, (int)o.Property("g").Value, (int)o.Property("b").Value));
            });
            Events.Add("tuning:Rotate", (object[] args) =>
            {
                var rotation = (int)args[0];
                vehicle.SetHeading(rotation);
            });
            Events.Add("tuning:buyColorPrimary", (object[] args) =>
            {
                Events.CallRemote("colorPrimaryPurchase", args[0]);
            });
            Events.Add("tuning:buyColorSecondary", (object[] args) =>
            {
                Events.CallRemote("colorSecondaryPurchase", args[0]);
            });
            Events.Add("tuning:buyTuningColor", (object[] args) =>
            {
                Events.CallRemote("colorNeonPurchase", args[0]);
            });
            Events.Add("tuning:Purchase", (object[] args) =>
            {
                Events.CallRemote("tuningPurchase", modType, modIndex, price, modName, wheelType, wheelIndex);
            });
            Events.Add("tuning:PlaySound", (object[] args) =>
            {
                RAGE.Game.Audio.PlaySoundFrontend(-1, "SultanRS_Upgrade", "JA16_Super_Mod_Garage_Sounds", true);
            });
            Events.Add("Tuning", (object[] args) => 
            {
                var veh = (Vehicle)args[0];
                var prim1 = (int)args[1];
                var prim2 = (int)args[2];
                var prim3 = (int)args[3];
                var sec1 = (int)args[4];
                var sec2 = (int)args[5];
                var sec3 = (int)args[6];
                veh.SetModKit(0);
                Player.LocalPlayer.Vehicle.SetVisible(false, false);
                VehicleMods vehicleMods = new VehicleMods();
                var vehMods = vehicleMods.Mods(veh);
                var wheelType = veh.GetWheelType();
                vehicle = new Vehicle(veh.Model, new Vector3(-338.138f, -137.2214f, 39.00965f), 125f, "", 255, false, 0, 0);
                vehicle.SetWheelType(wheelType);
                vehicle.SetCustomPrimaryColour(prim1, prim2, prim3);
                vehicle.SetCustomSecondaryColour(sec1, sec2, sec3);
                for(int i = 0 ;i < vehMods.Count; i++)
                {
                    vehicle.SetMod(i, vehMods[i], false);
                }
                var mods = VehicleMod.Mods(veh);
                string carMods = JsonConvert.SerializeObject(mods);
                CefWindow.ExecuteJs($"EventBus.$emit('vehicleTunes', '{carMods}');");
            });
        }
    }
}
