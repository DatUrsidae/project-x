using RAGE;
using System;

namespace Main.CEF {
    public class VehicleSpawner : HtmlWindow {
        public VehicleSpawner() {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "VehicleSpawner";
            WindowPath = "package://ServerUI/index.html#/VehicleSpawner";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady() {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents() {
            // Server -> Client -> CEF requests
            Events.Add("vehicles:Close", (object[] args) => {
                HideWindow();
            });
            Events.Add("vehicles:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("VehiclesOpen");
            });
            Events.Add("vehicles:setPlayerVehicles", (object[] args) => {
                CefWindow.ExecuteJs($"EventBus.$emit('setPlayerVehicles', '{args[0]}');");
            });
            Events.Add("vehicles:spawn", (object[] args) => {
                Events.CallRemote("vehicles:selected", args[0]);
            });
            Events.Add("vehicles:sell", (object[] args) =>
            {
                Events.CallRemote("vehicles:sell", args[0]);
            });
        }
    }
}
