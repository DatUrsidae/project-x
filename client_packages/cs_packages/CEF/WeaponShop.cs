using RAGE;
using System;

namespace Main.CEF
{
    public class WeaponShopWindow : HtmlWindow
    {
        public WeaponShopWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "WeaponShop";
            WindowPath = "package://ServerUI/index.html#/Weapon_Shop";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("weaponShop:Close", (object[] args) => {
                HideWindow();
            });
            Events.Add("weaponShop:Open", (object[] args) => {
                RenderWindow();
            });
            Events.Add("weaponShop:purchase", (object[] args) => {
                Events.CallRemote("OnWeaponPurchaseAttempt", args[0], args[1], args[2]);
            });
        }
    }
}
