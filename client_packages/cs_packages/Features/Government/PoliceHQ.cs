using System.Collections.Generic;
using RAGE;
using RAGE.Elements;

namespace ProjectX_Client.Features
{
    public class PoliceHQ : Events.Script
    {
        List<Marker> HQMarkers = new List<Marker>();
        public PoliceHQ()
        {
            Events.Add("Government", (object[] args) =>
            {
                HQMarkers.Add(new Marker(1, new Vector3(452.0917f, -980.1563f, 30.68961f - 1f), 1, new Vector3(), new Vector3(), new RGBA(200, 200, 50)));
                HQMarkers.Add(new Marker(1, new Vector3(445.8282f, -996.3215f, 30.68959f - 1f), 1, new Vector3(), new Vector3(), new RGBA(200, 200, 50)));
                HQMarkers.Add(new Marker(1, new Vector3(456.7452f, -987.4859f, 43.69164f - 1f), 1, new Vector3(), new Vector3(), new RGBA(200, 200, 50)));
            });
            Events.Add("DeleteJobs", (object[] args) =>
            {
                if (HQMarkers.Count != 0)
                {
                    foreach(var marker in HQMarkers)
                    {
                        marker.Destroy();
                    }
                }
            });
        }
    }
}
