using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectX_Client.Features.Jobs.Criminal
{
    public class Mission : Events.Script
    {
        public List<Marker> Markers { get; set; } = new List<Marker>();
        public Mission()
        {
            Events.Add("Mission:Spawn", (object[] args) =>
            {
                var vector = (Vector3)args[0];
                Markers.Add(new Marker(1, vector.Subtract(new Vector3(0, 0, 3)), 3, new Vector3(), new Vector3(), new RGBA(200, 50, 50)));
            });

            Events.Add("Mission:Delete", (object[] args) =>
            {
                foreach (var marker in Markers)
                {
                    marker.Destroy();
                }
            });
        }
    }
}
