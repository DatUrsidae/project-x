let chat =
{
	size: 0,
	history_limit: 30,
	container: null,
	input: null,
	enabled: false,
	active: true,
	global: false,
	local: false,
	faction: false
};

const HexPattern = /(#[0-9a-f]{6})(\s*[\w,\s\(\)'"\]\[\-\_\:\;\.\,\?]+\s*)/gi

function enableChatInput(enable)
{
	if(chat.active == false
		&& enable == true)
		return;
	
    if (enable != (chat.input != null))
	{
        //chat_printing = enable;

        mp.invoke("focus", enable);

        if (enable)
		{
            chat.input = $("#chat").append('<div><input id="chat_msg" type="text" /></div>').children(":last");
			chat.input.children("input").focus();
        } 
		else
		{
            chat.input.fadeOut('fast', function()
			{
                chat.input.remove();
				chat.input = null;
				chat.global = false;
				chat.local = false;
				chat.faction = false;
            });
        }
    }
}

function parseColors(str) {
	return str.replace(HexPattern, (s, g0, g1) => `<font style="color:${g0}">${g1}</font>`)
}

var chatAPI =
{
	push: (text) =>
	{
		chat.container.prepend("<li>" + parseColors(text) + "</li>");

		chat.size++;

		if (chat.size >= chat.history_limit)
		{
			chat.container.children(":last").remove();
		}
	},
	
	clear: () =>
	{
		chat.container.html("");
	},
	
	activate: (toggle) =>
	{
		if (toggle == false
			&& (chat.input != null))
			enableChatInput(false);
			
		chat.active = toggle;
	},
	
	show: (toggle) =>
	{
		if(toggle)
			$("#chat").show();
		else
			$("#chat").hide();
		
		chat.active = toggle;
	}
};

$(document).ready(function()
{
	chat.container = $("#chat ul#chat_messages");
	
    $(".ui_element").show();

    $("body").keydown(function(event)
	{
        if (event.which == 84 && chat.input == null
			&& chat.active == true)
		{
			chat.global = true;
            enableChatInput(true);
            event.preventDefault();
		}
		else if (event.which == 89 && chat.input == null
			&& chat.active == true)
			{
				chat.local = true;
				enableChatInput(true);
				event.preventDefault();
			}
		else if (event.which == 85 && chat.input == null
			&& chat.active == true)
			{
				chat.faction = true;
				enableChatInput(true);
				event.preventDefault();
			}
		else if (event.which == 13 && chat.input != null)
		{
            var value = chat.input.children("input").val();

            if (value.length > 0 && chat.global == true) 
			{
                if (value[0] == "/")
				{
                    value = value.substr(1);

					if (value.length > 0)
					{
						mp.invoke("command", value);
						chat.global = false;
					}
                }
				else
				{
					mp.trigger("globalMessage", value);
					chat.global = false;
                }
			}
			
			if (value.length > 0 && chat.local == true)
			{
				mp.trigger("localMessage", value);
				chat.local = false;
			}

			if (value.length > 0 && chat.faction == true)
			{
				mp.trigger("factionMessage", value);
				chat.faction = false;
			}

            enableChatInput(false);
        }
    });
});